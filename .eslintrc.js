/** @type {import('eslint').Linter.Config} */
module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    "eslint:recommended",
    "plugin:prettier/recommended",
    "plugin:mocha/recommended",
  ],
  plugins: ["mocha"],
  overrides: [
    {
      env: {
        node: true,
      },
      files: [".eslintrc.{js,cjs}"],
      parserOptions: {
        sourceType: "script",
      },
    },
  ],
  parserOptions: {
    ecmaVersion: 2023,
    sourceType: "module",
  },
  rules: {
    "no-unused-vars": [
      "error",
      { varsIgnorePattern: "^_", argsIgnorePattern: "^_" },
    ],
    "prettier/prettier": [
      "error",
      {},
      {
        usePrettierrc: true,
      },
    ],
  },
  globals: {
    Babele: "readonly",
    CONFIG: "readonly",
    Dialog: "readonly",
    foundry: "readonly",
    fromUuid: "readonly",
    fromUuidSync: "readonly",
    game: "readonly",
    Hooks: "readonly",
    loadTemplates: "readonly",
    pf1: "readonly",
    renderTemplate: "readonly",
    saveDataToFile: "readonly",
    ui: "readonly",
  },
};
