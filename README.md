# Pathfinder 1e: Español

![Pathfinder 1e System](https://img.shields.io/badge/Pathfinder%20System-36.54%25-blue?style=flat-square&link=https%3A%2F%2Fgitlab.com%2Flecuay%2Ffoundryvtt-pf1e-es%23compendiums-traducidos)
![PF-Content](https://img.shields.io/badge/PF%20Content-11.04%25-brown?style=flat-square&link=https%3A%2F%2Fgitlab.com%2Flecuay%2Ffoundryvtt-pf1e-es%23compendiums-traducidos)
![Archetypes](https://img.shields.io/badge/PF1e%20Archetypes%20%26%20Prestige-0.07%25-darkcyan?style=flat-square&link=https%3A%2F%2Fgitlab.com%2Flecuay%2Ffoundryvtt-pf1e-es%23compendiums-traducidos)

![Total translation](https://img.shields.io/badge/Total-16.78%25-green?style=flat-square&link=https%3A%2F%2Fgitlab.com%2Flecuay%2Ffoundryvtt-pf1e-es%23compendiums-traducidos)

Traducciones al español de los compendios de Pathfinder 1e

> Este proyecto parte de un fork de las traducciones en alemán (<https://gitlab.com/arkanamirium/foundryvtt-pf1e-de>).
>
> ---
>
> This project is a fork from german translations (<https://gitlab.com/arkanamirium/foundryvtt-pf1e-de>).

## Requisitos

* [FoundryVTT](https://foundryvtt.com/)
* [Pathfinder 1e for Foundry VTT](https://gitlab.com/Furyspark/foundryvtt-pathfinder1)
* [Babele](https://foundryvtt.com/packages/babele/)

## Instalación

* Acceder a la configuración de Foundry VTT.
* Instalar los requisitos previos y este módulo mediante el instalador de módulos.
* Activar los módulos en el mundo de juego correspondiente.

## Compendiums traducidos

### Sistema Pathfinder 1e

|Compendium                         |Porcentaje|Entradas traducidas|
|:---------------------------------:|----------|-------------------|
|Armaduras y escudos                |98.48%    |65                 |
|Aptitudes de compañero             |100.00%   |10                 |
|Armas y munición                   |51.90%    |246                |
|Buffs comunes                      |98.18%    |54                 |
|Clases                             |59.18%    |29                 |
|Conjuros                           |40.51%    |1227               |
|Dotes                              |100%      |265                |
|Habilidades de clase               |23.85%    |1074               |
|Habilidades de monstruos           |100%      |22                 |
|Plantillas de monstruos            |1.06%     |2                  |
|Mythic Paths                       |0.00%     |0                  |
|Objetos                            |27.88%    |259                |
|Pathfinder 1e Rules                |100%      |4                  |
|DG Raciales                        |100%      |13                 |
|Razas                              |16.05%    |13                 |
|Roll tables                        |0.00%     |0                  |
|Ultimate Equipment                 |0.00%     |0                  |

### [Pathfinder 1e Content](https://foundryvtt.com/packages/pf-content)

|Compendium                         |Porcentaje|Entradas traducidas|
|:---------------------------------:|----------|-------------------|
|Aptitudes de compañero animal      |100%      |12                 |
|Animal Companions                  |0.00%     |0                  |
|Artifacts                          |0.00%     |0                  |
|Bienes y servicios                 |17.00%    |84                 |
|Buffs                              |100%      |186                |
|Deities                            |0.00%     |0                  |
|Dotes                              |14.34%    |473                |
|Eidolon Forms                      |1.11%     |0                  |
|Evoluciones de eidolón             |88.07%    |96                 |
|Encounter Tables                   |0.00%     |0                  |
|Familiars                          |0.00%     |0                  |
|GM Quick Reference                 |0.00%     |0                  |
|Habilidades de clase               |9.77%     |88                 |
|Kingdom Building - Buildings       |0.00%     |0                  |
|Kingdom Building - Rules GM        |0.00%     |0                  |
|Kingdom Building - Rules Player    |0.00%     |0                  |
|Kingdom Building - Tables          |0.00%     |0                  |
|Magic Items                        |0.00%     |0                  |
|Magic Items Tables                 |0.00%     |0                  |
|Maladies                           |0.00%     |0                  |
|Objetos                            |3.97%     |44                 |
|Objetos mágicos                    |9.09%     |71                 |
|Objetos maravillosos               |4.85%     |139                |
|Occult Rituals                     |0.00%     |0                  |
|Racial Traits                      |0.00%     |0                  |
|Rasgos                             |3.71%     |79                 |
|Reglas universales de los monstruos|44.22%    |65                 |
|Rules                              |0.00%     |0                  |
|Special Qualities                  |0.00%     |0                  |
|Tables                             |0.00%     |0                  |
|Technology                         |0.00%     |0                  |
|Traps and Haunts                   |0.00%     |0                  |

### [Archetypes](https://gitlab.com/tristangoucher/pf1e-archetypes)

|Compendium                         |Porcentaje|Entradas traducidas|
|:---------------------------------:|----------|-------------------|
|Arquetipos                         |0.08%     |1                  |
|Rasgos de arquetipo                |0.06%     |3                  |
|Clases de prestigio                |0.84%     |1                  |
|Rasgos de prestigio                |0.00%     |0                  |

## Disclaimer

> This FoundryVTT module uses trademarks and/or copyrights owned by Paizo Inc., used under Paizo's Community Use Policy
> (paizo.com/communityuse). We are expressly prohibited from charging you to use or access this content. This FoundryVTT module is not
> published, endorsed, or specifically approved by Paizo. For more information about Paizo Inc. and Paizo products, visit paizo.com.
