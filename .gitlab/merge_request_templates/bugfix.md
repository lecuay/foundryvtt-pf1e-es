# Resumen

## Tu lista a completar para esta Merge request

- [ ] Asegúrate que está solicitando una **integración (_merge_) de un `bugfix/`**. ¡No pidas la integración de tu `master`!
- [ ] Asegúrate de que estás solicitando una integración (_merge_) contra la **rama `develop`**. Además debería empezar **tu rama** desde **nuestra `develop`**.
- [ ] Comprueba que el commit (o todos los commits) tienen la estructura correcta. Puede validarlos [aquí](https://commitlint.io).
- [ ] ¡Obtén un verde en la _pipeline_!

### Otras comprobaciones

- [ ] Actualiza el `README.md` o la documentación si es necesario.
- [ ] Recuerda incluirte en `CONTRIBUTORS.md`.

## Descripción

Por favor, provee una descripción para tu _merge request_.

## ¿Implementas un _breaking change_?

- [ ] Sí
- [ ] No
