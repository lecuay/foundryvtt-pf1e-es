# Resumen

<!-- Pon un resumen general del problema en el Título -->
<!--
    Todas la secciones señaladas con [Opcional] pueden ser borradas si no son
    necesarias o relevantes para el report del bug.
-->

## Comportamiento esperado

<!-- Cuéntanos qué debería pasar o qué esperabas que pasara -->

## Comportamiento actual

<!-- Cuéntanos qué ocurre en lugar del comportamiento esperado -->

## [Opcional] Posible solución

<!-- Si ya tienes alguna idea sobre cómo solucionar el problema, háznoslo saber -->

## [Opcional] Pasos para reproducir el problema

<!--
    Si es relevante (por ejemplo para un problema con muchos pasado) proveed un ejemplo
    en vivo (un conjunto de GIFs o un vídeo corto). Sino, enumera los pasos para
    reproducir este error.
-->

1. Lorem
2. ipsum
