/** @type {import('jest').Config} */
const config = {
  setupFilesAfterEnv: ["<rootDir>/tests/setupFoundryGlobals.js"],
  verbose: true,
};

module.exports = config;
