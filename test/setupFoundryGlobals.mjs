export const mochaHooks = {
  beforeAll(done) {
    global.game = {
      i18n: {
        localize: (/** @type {string} */ text) => {
          switch (text.replace("PF1.", "")) {
            case "Use":
              return "Usar";
            case "Attack":
              return "Ataque";
            default:
              return text;
          }
        },
        translations: {
          PF1: {
            Attack: "Ataque",
            Use: "Usar",
          },
        },
      },
      settings: {
        get: (/** @type {string} */ module, /** @type {string} */ setting) => {
          if (module === "pf1e-es" && setting === "enableDebug") {
            return true;
          } else {
            throw new Error("Trying to access settings out of scope");
          }
        },
      },
    };
    global.ui = {
      notifications: {
        error: () => console.error,
      },
    };

    done();
  },
  afterAll(done) {
    delete global.game;

    done();
  },
};
