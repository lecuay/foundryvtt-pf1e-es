import { expect } from "chai";

import { tScriptCalls } from "../scripts/scriptCallsConverter.mjs";

describe("tScriptCalls Suite", function () {
  const DUMMY_SCRIPT_CALLS = [
    {
      _id: "jqq7XX7qM19jsC6N",
      name: "Toggle Actor Size (Enlarge)",
      img: "icons/svg/dice-target.svg",
      value:
        "if (!actor) return;\n\nconst defaultSize = 'med'; // Fallback if no size info is found\nconst naturalSize = actor.race?.system.size || defaultSize;\nconst currentSize = actor.system.traits?.size || defaultSize;\n\nlet newSize = naturalSize;\nif (state) {\n\tconst steps = Object.keys(pf1.config.sizeChart);\n\tconst current = steps.indexOf(currentSize);\n\tconst adjusted = Math.clamped(current + 1, 0, steps.length - 1);\n\tnewSize = steps[adjusted];\n}\n\nactor.update({ 'system.traits.size': newSize });\n",
      category: "toggle",
      hidden: false,
    },
  ];
  const DUMMY_TRANSLATIONS = [
    {
      name: "Activar tamaño del actor (Agrandar)",
    },
  ];

  it("Single script call", function () {
    let scriptCalls = JSON.parse(JSON.stringify(DUMMY_SCRIPT_CALLS));
    let translations = JSON.parse(JSON.stringify(DUMMY_TRANSLATIONS));

    const translated = tScriptCalls(scriptCalls, translations);

    translated.forEach((translation, index) => {
      expect(translation.name).to.equal(translations[index].name);
    });
  });
});
