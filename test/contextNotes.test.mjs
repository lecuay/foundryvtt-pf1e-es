import { expect } from "chai";
import * as sinon from "sinon";

import { contextNotes } from "../scripts/contextNotes.mjs";

describe("contextNotes Suite", function () {
  /** @type {Array.<ContextNote>} */
  const DUMMY_CONTEXT_NOTES = [
    {
      formula: "",
      operator: "add",
      target: "misc",
      subTarget: "ac",
      modifier: "",
      priority: 0,
      value: 0,
      text: "+[[4]] Dodge vs Giants",
    },
    {
      formula: "",
      operator: "add",
      target: "savingThrows",
      subTarget: "allSavingThrows",
      modifier: "",
      priority: 0,
      value: 0,
      text: "+[[2]] Racial vs Poisons, Spells and Spell-likes",
    },
    {
      formula: "",
      operator: "add",
      target: "misc",
      subTarget: "cmd",
      modifier: "",
      priority: 0,
      value: 0,
      text: "+[[4]] Racial vs Bull Rush and Trip while on ground",
    },
    {
      formula: "",
      operator: "add",
      target: "skill",
      subTarget: "skill.apr",
      modifier: "",
      priority: 0,
      value: 0,
      text: "+[[2]] Racial to Appraise Items with Gems",
    },
    {
      formula: "",
      operator: "add",
      target: "skill",
      subTarget: "skill.per",
      modifier: "",
      priority: 0,
      value: 0,
      text: "+[[2]] to Notice Unusual Stonework",
    },
    {
      formula: "",
      operator: "add",
      target: "attacks",
      subTarget: "attack",
      modifier: "",
      priority: 0,
      value: 0,
      text: "+[[1]] Racial vs Humanoids (Orc, Goblinoid)",
    },
  ];

  /** @type {Array.<TranslatedContextNote>} */
  const DUMMY_TRANSLATED_CONTEXT_NOTES = [
    { text: "+[[4]] Esquiva vs Gigantes" },
    { text: "+[[2]] Racial vs Venenos, Conjuros y Aptitudes sortílegas" },
    { text: "+[[4]] Racial vs Embestida y Derribo estando de pie" },
    {
      text: "+[[2]] Racial a Tasación en objetos con gemas o metales preciosos",
    },
    { text: "+[[2]] en observar un trabajo de cantería inusual" },
    { text: "+[[1]] Racial vs Humanoides (Orco, Goblin)" },
  ];

  it("Multiple context notes are correctly translated", function () {
    /** @type {Array.<ContextNote>} */
    let contextNotesData = JSON.parse(JSON.stringify(DUMMY_CONTEXT_NOTES));
    /** @type {Array.<TranslatedContextNote>} */
    let translatedContextNotes = JSON.parse(
      JSON.stringify(DUMMY_TRANSLATED_CONTEXT_NOTES)
    );
    const translations = contextNotes(contextNotesData, translatedContextNotes);

    translations.forEach((ctxNote, idx) => {
      expect(ctxNote.text).to.equal(translatedContextNotes[idx].text);
    });
  });

  it("Missing context notes returns originals", function () {
    const consoleErrorSpy = sinon.spy(console, "error");

    /** @type {Array.<ContextNote>} */
    let contextNotesData = JSON.parse(JSON.stringify(DUMMY_CONTEXT_NOTES));
    /** @type {Array.<TranslatedContextNote>} */
    let translatedContextNotes = JSON.parse(
      JSON.stringify(DUMMY_TRANSLATED_CONTEXT_NOTES)
    );
    const randomTranslationsToCutIdx = Math.round(
      Math.random() * (translatedContextNotes.length - 1) + 1
    );
    const slicedTranslations = translatedContextNotes.slice(
      randomTranslationsToCutIdx
    );
    const translations = contextNotes(contextNotesData, slicedTranslations);

    translations.forEach((ctxNote, idx) => {
      expect(ctxNote.text).to.equal(contextNotesData[idx].text);
    });

    expect(
      consoleErrorSpy.withArgs(
        `Faltan traducciones para las Notas de contexto -> ${slicedTranslations.length}/6`
      ).calledOnce
    ).to.be.true;
  });
});
