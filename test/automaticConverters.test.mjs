import { expect } from "chai";

import { translateSubSchool } from "../scripts/automaticConverters.mjs";

describe("translateSubSchool Suite", function () {
  const tests = [["creation or calling", "creación o llamada"]];
  const testTranslateSchool = ([args, expected]) =>
    function () {
      expect(translateSubSchool(args)).to.equal(expected);
    };

  it("creation or calling", testTranslateSchool(tests[0]));
});
