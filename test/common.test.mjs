/* eslint-disable array-element-newline */
import { expect } from "chai";

import {
  tArea,
  tDuration,
  tEffect,
  tSavingThrowType,
  tTarget,
  tActionName,
} from "../scripts/common.mjs";

describe("common.mjs - Automatic Converters", function () {
  describe("tActionName Suite", function () {
    const tests = [
      ["Use", "Usar"],
      ["Attack", "Ataque"],
      ["Ray", "Rayo"],
      ["Ray 1", "Rayo 1"],
      ["Unknown", "Unknown"],
      ["Bite", "Mordisco"],
      ["Creature", "Criatura"],
      ["Creatures", "Criaturas"],
      ["Heal", "Curar"],
      ["Harm Undead", "Dañar muertos vivientes"],
    ];

    // eslint-disable-next-line mocha/no-setup-in-describe
    tests.forEach(([original, translation]) => {
      it(`'${original}' => '${translation}'`, function () {
        expect(tActionName(original)).to.equal(translation);
      });
    });
  });

  describe("tSavingThrowType Suite", function () {
    const tests = [
      ["Will partial", "Voluntad parcial"],
      ["Will halves damage", "Voluntad reduce el daño a la mitad"],
      [
        "Will negates (harmless, object)",
        "Voluntad niega (inofensivo, objeto)",
      ],
      [
        "Will disbelief, then Fortitude partial; see text",
        "Voluntad descree, luego Fortaleza parcial; ver texto",
      ],
      ["Reflex halves", "Reflejos mitad"],
      [
        "Reflex halves splash damage",
        "Reflejos reduce el daño de salpicadura a la mitad",
      ],
      ["Fortitude negates", "Fortaleza niega"],
      [
        "Fortitude negates (harmless, object)",
        "Fortaleza niega (inofensivo, objeto)",
      ],
      ["none and Will negates (object)", "ninguna y Voluntad niega (objeto)"],
      ["none or Will negates; see text", "ninguna o Voluntad niega; ver texto"],
      [
        "Fortitude negates (harmless) or Will disbelieves (if interacted with)",
        "Fortaleza niega (inofensivo) o Voluntad descree (si se interactúa con el conjuro)",
      ],
      [
        "Will disbelief (if interacted with); varies; see text",
        "Voluntad descree (si se interactúa con el conjuro); varía; ver texto",
      ],
      ["special, see below", "especial, ver más abajo"],
    ];

    // eslint-disable-next-line mocha/no-setup-in-describe
    tests.forEach(([original, translation]) => {
      it(`'${original}' => '${translation}'`, function () {
        expect(tSavingThrowType(original)).to.equal(translation);
      });
    });
  });

  describe("tTarget Suite", function () {
    const tests = [
      ["you", "tú"],
      ["your eidolon", "tu eidolon"],
      [
        "up to four creatures, no two of which can be more than 30 ft. apart",
        "hasta cuatro criaturas, 2 receptores cualesquiera no pueden distar más de 30 pies (9 m)",
      ],
      ["one creature", "una criatura"],
      ["one animal", "un animal"],
      ["creature touched", "criatura tocada"],
      ["creatures touched", "criaturas tocadas"],
      ["creature or object touched", "criatura u objeto tocado"],
      ["armor or shield touched", "armadura o escudo tocado"],
      [
        "one willing creature; see below",
        "una criatura voluntaria; ver más abajo",
      ],
      ["one willing creature touched", "una criatura voluntaria tocada"],
      ["living creature touched", "criatura viva tocada"],
      [
        "one living creature of 6 HD or less",
        "una criatura viva de 6 DG o menos",
      ],
      ["up to one touched creature/level", "hasta una criatura tocada/nivel"],
      [
        "one humanoid creature of 4 HD or less",
        "una criatura humanoide de 4 DG o menos",
      ],
      [
        "weapon touched or 50 projectiles (all of which must be together at the time of casting)",
        "arma tocada o 50 proyectiles (deben estar todos juntos en el momento del lanzamiento)",
      ],
      [
        "one weapon/level, no two of which can be more than 20 ft. apart",
        "un arma/nivel, 2 receptores cualesquiera no pueden distar más de 20 pies (6 m)",
      ],
      ["one creature", "una criatura"],
      ["one creature touched/level", "una criatura tocada/nivel"],
      ["one creature touched/2 levels", "una criatura tocada/2 niveles"],
      [
        "one good-aligned creature/3 levels",
        "una criatura alineada con el bien/3 niveles",
      ],
      [
        "nonlawful creatures in a 40-ft.-radius spread centered on you",
        "criaturas no legales en una expansión de 40 pies (12 m) de radio centrada en ti",
      ],
      [
        "one willing living creature per 2 levels, no two of which may be more than 30 ft. apart",
        "una criatura viva voluntaria cada 2 niveles, 2 receptores cualesquiera no pueden distar más de 30 pies (9 m)",
      ],
      [
        "one willing living creature per three levels, no two of which may be more than 30 ft. apart",
        "una criatura viva voluntaria cada tres niveles, 2 receptores cualesquiera no pueden distar más de 30 pies (9 m)",
      ],
      [
        "you plus one additional willing creature touched per two caster levels",
        "tú más una criatura voluntaria adicional tocada cada dos niveles de lanzador",
      ],
      [
        "up to four creatures, no two of which can be more than 30 ft. apart",
        "hasta cuatro criaturas, 2 receptores cualesquiera no pueden distar más de 30 pies (9 m)",
      ],
      [
        "several living creatures, no two of which may be more than 30 ft. apart",
        "varias criaturas vivas, 2 receptores cualesquiera no pueden distar más de 30 pies (9 m)",
      ],
      [
        "you or a creature or object weighing no more than 100 lbs./level",
        "tú o una criatura u objeto que no pese más de 100 libras (45 kg)/nivel",
      ],
      ["flask of water touched", "frasco de agua tocado"],
      ["until discharged", "hasta ser descargado"],
      ["see text", "ver texto"],
      [
        "one elemental or outsider with 6 HD or less",
        "un elemental o ajeno con 6 DG o menos",
      ],
      [
        "one living creature with 5 or fewer HD",
        "una criatura viva con 5 o menos DG",
      ],
      [
        "up to three elementals or outsiders",
        "hasta tres elementales o ajenos",
      ],
      [
        "you and touched objects or other touched willing creatures",
        "tú y objetos tocados u otras criaturas voluntarias tocadas",
      ],
      [
        "you plus one additional willing creature touched per two caster levels",
        "tú más una criatura voluntaria adicional tocada cada dos niveles de lanzador",
      ],
      [
        "you plus one willing creature per three levels, no two of which can be more than 30 ft. apart",
        "tú más una criatura voluntaria cada tres niveles, 2 receptores cualesquiera no pueden distar más de 30 pies (9 m)",
      ],
      [
        "you and one other touched creature per three levels",
        "tú y otra criatura tocada cada tres niveles",
      ],
      [
        "one living creature/level, no two of which can be more than 30 ft. apart",
        "una criatura viva/nivel, 2 receptores cualesquiera no pueden distar más de 30 pies (9 m)",
      ],
      [
        "creature or creatures touched (up to one per level)",
        "criatura o criaturas tocadas (hasta una por nivel)",
      ],
      [
        "one touched object weighing no more than 10 lbs.",
        "un objeto tocado que no pese más de 10 libras (4,5 kg)",
      ],
      ["one humanoid creature per level", "una criatura humanoide por nivel"],
      ["two creatures touched", "dos criaturas tocadas"],
      [
        "you plus one willing creature per 3 levels, no two of which can be more than 30 ft. apart",
        "tú más una criatura voluntaria cada 3 niveles, 2 receptores cualesquiera no pueden distar más de 30 pies (9 m)",
      ],
      ["one melee weapon", "un arma cuerpo a cuerpo"],
      ["eidolon touched", "eidolón tocado"],
      ["one eidolon", "un eidolón"],
      [
        "one creature/level in a 20-ft.-radius burst centered on you",
        "una criatura/nivel en una explosión de 20 pies (6 m) de radio centrada en ti",
      ],
      [
        "one creature plus one additional creature per four levels, no two of which can be more than 30 ft. apart",
        "una criatura más una criatura adicional cada cuatro niveles, 2 receptores cualesquiera no pueden distar más de 30 pies (9 m)",
      ],
      [
        "creature touched or all creatures within 5 ft.; see text",
        "criatura tocada o todas las criaturas en 5 pies (1,5 m); ver texto",
      ],
      [
        "one or more living creatures within a 10-ft.-radius burst",
        "una o más criaturas vivas en una explosión de 10 pies (3 m) de radio",
      ],
      [
        "living creatures, no two of which can be more than 60 ft. apart",
        "criaturas vivas, 2 receptores cualesquiera no pueden distar más de 60 pies (18 m)",
      ],
      [
        "willing corporeal creature touched",
        "criatura corporal voluntaria tocada",
      ],
      [
        "up to 10 cu. ft./level; see text",
        "hasta 10 pies cúbicos (0,28 m3)/nivel; ver texto",
      ],
      [
        "up to one willing creature per level, all within 30 ft. of each other.",
        "hasta una criatura voluntaria por nivel, 2 receptores cualesquiera no pueden distar más de 30 pies (9 m).",
      ],
      [
        "5-ft.-radius spread; or one solid object or one crystalline creature",
        "expansión de 5 pies (1,5 m) de radio; o un objeto sólido o una criatura cristalina",
      ],
      [
        "One or more creatures, no two of which can be more than 30 ft. apart",
        "una o más criaturas, 2 receptores cualesquiera no pueden distar más de 30 pies (9 m)",
      ],
      [
        "melee weapon touched (see text)",
        "arma cuerpo a cuerpo tocada (ver texto)",
      ],
      ["one extraplanar creature", "una criatura extraplanaria"],
      ["any number of creatures", "cualquier número de criaturas"],
      [
        "up to 2 HD/level of plant creatures, no two of which can be more than 30 ft. apart",
        "hasta 2 DG/nivel de criaturas tipo planta, 2 receptores cualesquiera no pueden distar más de 30 pies (9 m)",
      ],
      [
        "creature or creatures touched (up to one/level)",
        "criatura o criaturas tocadas (hasta una/nivel)",
      ],
      [
        "one touched object weighing up to 5 lbs./level",
        "un objeto tocado que no pese más de 5 libras (2,3 kg)/nivel",
      ],
      [
        "one or more creatures, no two of which can be more than 30 ft. apart",
        "una o más criaturas, 2 receptores cualesquiera no pueden distar más de 30 pies (9 m)",
      ],
      [
        "allies within a 20-ft.-radius burst centered on you",
        "aliados en una explosión de 20 pies (6 m) de radio centrada en ti",
      ],
      ["dead creature touched", "criatura muerta tocada"],
      [
        "one object or one 5-ft. square",
        "un objeto o un cuadrado de 5 pies (1,5 m)",
      ],
      ["allies within range", "aliados en rango"],
      ["10-ft. radius burst", "explosión de 10 pies (3 m) de radio"],
      ["one construct or object", "un constructo u objeto"],
    ];

    // eslint-disable-next-line mocha/no-setup-in-describe
    tests.forEach(([original, translation]) => {
      it(`'${original}' => '${translation}'`, function () {
        expect(tTarget(original)).to.equal(translation);
      });
    });
  });

  describe("tArea Suite", function () {
    const tests = [
      ["cone-shaped emanation", "emanación en forma de cono"],
      ["cone-shaped burst", "explosión en forma de cono"],
      [
        "The caster and all allies within a 50-ft. burst, centered on the caster",
        "El lanzador y todos sus aliados en una explosión de 50 pies (15 m), centrada en el lanzador",
      ],
      [
        "The caster and all allies within a 30-ft.-radius burst, centered on the caster",
        "El lanzador y todos sus aliados en una explosión de 30 pies (9 m) de radio, centrada en el lanzador",
      ],
      [
        "30-ft. line, 20-ft. cone-shaped spread, or 10-ft.-radius spread (see text)",
        "línea de 30 pies (9 m), expansión de 20 pies (6 m) en forma de cono, o expansión de 10 pies (3 m) de radio (ver texto)",
      ],
      ["20-ft.-radius emanation", "emanación de 20 pies (6 m) de radio"],
      [
        "30-ft.-radius burst centered on you",
        "explosión de 30 pies (9 m) de radio centrada en ti",
      ],
      [
        "all allies and foes within a 40-ft.-radius burst centered on you",
        "todos los aliados y enemigos en una explosión de 40 pies (12 m) de radio centrada en ti",
      ],
      [
        "10-ft.-radius emanation from touched creature",
        "emanación de 10 pies (3 m) de radio desde la criatura tocada",
      ],
      [
        "10-ft.-radius emanation centered on you",
        "emanación de 10 pies (3 m) de radio centrada en ti",
      ],
      [
        "10-ft.-radius spherical emanation, centered on you",
        "emanación esférica de 10 pies (3 m) de radio, centrada en ti",
      ],
      [
        "object touched or up to 5 sq. ft./level",
        "objeto tocado o de hasta 5 pies cuadrados (0,46 m2)/nivel",
      ],
      [
        "circle, centered on you, with a radius of 400 ft. + 40 ft./level",
        "círculo, centrado en ti, con un radio de 400 pies (120 m) + 40 pies (12 m)/nivel",
      ],
      [
        "30-ft. line, 20-ft. cone-shaped spread, or 10-ft.-radius spread (see text)",
        "línea de 30 pies (9 m), expansión de 20 pies (6 m) en forma de cono, o expansión de 10 pies (3 m) de radio (ver texto)",
      ],
      [
        "up to 10-ft.-radius/level emanation centered on you",
        "emanación de hasta 10 pies (3 m) de radio/nivel centrada en ti",
      ],
      [
        "creatures and objects within a 5-ft.-radius burst",
        "criaturas y objetos en una explosión de 5 pies (1,5 m) de radio",
      ],
      [
        "cylinder (10-ft. radius, 40-ft. high)",
        "cilindro (10 pies (3 m) de radio, 40 pies (12 m) de alto)",
      ],
      [
        "living creatures within a 10-ft.-radius burst",
        "criaturas vivas en una explosión de 10 pies (3 m) de radio",
      ],
      [
        "nonevil creatures in a 40-ft.-radius spread centered on you",
        "criaturas no malignas en una expansión de 40 pies (12 m) de radio centrada en ti",
      ],
      ["2-mile-radius circle", "círculo de 2 millas (3,2 km) de radio"],
      [
        "2-mile-radius circle, centered on you; see text",
        // FIXME: This translation is gender wrong
        "círculo de 2 millas (3,2 km) de radio, centrada en ti; ver texto",
      ],
      [
        "5-ft.-radius spread; or one solid object or one crystalline creature",
        "expansión de 5 pies (1,5 m) de radio; o un objeto sólido o una criatura cristalina",
      ],
      ["15 ft. Cone", "cono de 15 pies (4,6 m)"],
      [
        "one or more living creatures within a 10-ft.-radius burst",
        "una o más criaturas vivas en una explosión de 10 pies (3 m) de radio",
      ],
    ];

    // eslint-disable-next-line mocha/no-setup-in-describe
    tests.forEach(([original, translation]) => {
      it(`'${original}' => '${translation}'`, function () {
        expect(tArea(original)).to.equal(translation);
      });
    });
  });

  describe("tDuration Suite", function () {
    const tests = [
      ["see text", "ver texto"],
      ["permanent; see text", "permanente; ver texto"],
      ["concentration", "concentración"],
      ["permanent until discharged", "permanente hasta ser descargado"],
      ["24 hours", "24 horas"],
      [
        "1 minute/level (D) and instantaneous",
        "1 minuto/nivel (D) e instantáneo",
      ],
      ["1 round/level", "1 asalto/nivel"],
      ["1 round/level (D)", "1 asalto/nivel (D)"],
      [
        "1 round/level or until discharged",
        "1 asalto/nivel o hasta ser descargado",
      ],
      [
        "concentration, up to 1 round/level",
        "concentración, hasta 1 asalto/nivel",
      ],
      ["1 hour/level", "1 hora/nivel"],
      [
        "1 hour/level or until completed (D)",
        "1 hora/nivel o hasta ser completada (D)",
      ],
      [
        "1 day/level or until discharged (D)",
        "1 día/nivel o hasta ser descargado (D)",
      ],
      ["one day/level", "un día/nivel"],
      [
        "10 minutes/level, then 1 hour/level or until completed (D); see text",
        "10 minutos/nivel, luego 1 hora/nivel o hasta ser completada (D); ver texto",
      ],
      ["10 min./level", "10 minutos/nivel"],
      [
        "1 hour/level or until you return to your body",
        "1 hora/nivel o hasta volver a tu cuerpo",
      ],
      [
        "instantaneous plus 1 minute/level (see text)",
        "instantáneo más 1 minuto/nivel (ver texto)",
      ],
      ["5 rounds or less; see text", "5 asaltos o menos; ver texto"],
      [
        "instantaneous or concentration (up to 1 round/level); see text",
        "instantáneo o concentración (hasta 1 asalto/nivel); ver texto",
      ],
      [
        "instantaneous (1 round); see text",
        "instantáneo (1 asalto); ver texto",
      ],
      [
        "1 minute/level (D), special see below",
        "1 minuto/nivel (D), especial ver más abajo",
      ],
      [
        "1 round/level (D) and concentration + 3 rounds; see text",
        "1 asalto/nivel (D) y concentración + 3 asaltos; ver texto",
      ],
      ["1 round + 1 round/4 levels", "1 asalto + 1 asalto/4 niveles"],
      [
        "30 minutes and 2d6 rounds; see text",
        "30 minutos y 2d6 asaltos; ver texto",
      ],
      [
        "instantaneous (1d4 rounds); see text",
        "instantáneo (1d4 asaltos); ver texto",
      ],
      [
        "concentration + 2 rounds or until triggered",
        "concentración + 2 asaltos o hasta dispararse",
      ],
      [
        "1 round/level or until discharged, whichever comes first",
        "1 asalto/nivel o hasta ser descargado, lo que suceda antes",
      ],
      ["until expended or 10 min./level", "hasta agotarse o 10 minutos/nivel"],
    ];

    // eslint-disable-next-line mocha/no-setup-in-describe
    tests.forEach(([original, translation]) => {
      it(`'${original}' => '${translation}'`, function () {
        expect(tDuration(original)).to.equal(translation);
      });
    });
  });

  describe("tEffect Suite", function () {
    const tests = [
      ["ray", "rayo"],
      ["one or more rays", "uno o más rayos"],
      ["one summoned creature", "una criatura convocada"],
      ["magical sensor", "sensor mágico"],
      ["see text", "ver texto"],
      ["one symbol", "un símbolo"],
      [
        "fog spreads in 20-ft. radius",
        "bruma que se expande en un radio de 20 pies (6 m)",
      ],
      [
        "cloud spreads in 20-ft. radius, 20 ft. high",
        "nube que se expande en un radio de 20 pies (6 m), 20 pies (6 m) de alto",
      ],
      [
        "fog spreads in 20-ft. radius, 20 ft. high",
        "bruma que se expande en un radio de 20 pies (6 m), 20 pies (6 m) de alto",
      ],
      [
        "cylinder (10-ft. radius, 40-ft. high)",
        "cilindro (10 pies (3 m) de radio, 40 pies (12 m) de alto)",
      ],
      [
        "wall up to 10 ft./level long and 5 ft./level high (S)",
        "muro de hasta 10 pies (3 m)/nivel de largo y 5 pies (1,5 m)/nivel de alto (Mo)",
      ],
    ];

    // eslint-disable-next-line mocha/no-setup-in-describe
    tests.forEach(([original, translation]) => {
      it(`'${original}' => '${translation}'`, function () {
        expect(tEffect(original)).to.equal(translation);
      });
    });
  });
});
