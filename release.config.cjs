/**
 * @type {import('semantic-release').GlobalConfig}
 */
module.exports = {
  branches: ["main", "develop"],
  tagFormat: "${version}",
  plugins: [
    "@semantic-release/commit-analyzer",
    [
      "@semantic-release/exec",
      {
        verifyReleaseCmd: "echo ${nextRelease.version} > .VERSION",
      },
    ],
  ],
  dryRun: false,
  ci: true,
};
