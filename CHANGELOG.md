## 1.14.0 (2025-03-05)

### Añadido (9 cambios)

- [feat(lang/es/pf-content.pf-wondrous.json): objetos maravillosos - letra D - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@085c8bee5429451d903fe13342ef612d95af5232)
- [feat(lan): objetos maravillosos - letra C - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@a202d10ca451d99876b87bc0c951853e801767ef)
- [feat(lan): resolve "Rasgos de fe - GJA"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@fa567ea066cf18bca0e8e217251e772ecca7ef9c) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!212))
- [feat(lan): resolve "Rasgos regionales - GJA"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@8e1fb84420849514211a5bf4a792cb4637ac10b6) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!211))
- [feat(lan): resolve "Rasgos raciales - GJA"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@39eb60f3d47a8a156fc0131e4375ba5c0897c3d2) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!210))
- [feat(lan): resolve "Rasgos de campaña - GJA"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@4402a5192ad7e2c730611c4d5b6fe4d9ad562869) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!209))
- [feat(lan): resolve "Rasgos sociales - GJA"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@abaa9c21ec0b0403de7aa5dcda29999d90749e20) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!208))
- [feat(lan): resolve "Rasgos mágicos - GJA"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@5cd4b5e0151938ccd9cf3259a9c6bddd85d8d74f) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!207))
- [feat(lan): resolve "Rasgos de combate - GJA"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@7068af28f6ee378c984fa40088b107299aa6f03a) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!206))

## 1.13.1 (2025-03-03)

### Arreglado (4 cambios)

- [fix(lang/es/pf1.class-abilities.json): enlaces mal traducidos de poderes de furia - GJA](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@749870656bfd1e366e2099b8b2706907c0e93737)
- [fix:(lan): resolve "Faltan poderes de furia - CD"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@c99a61f3b4de4f4aa916c89080f5e3ec9adee3ff) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!204))
- [fix(lan): resolve "Aptitudes de los familiares"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@d7748a9ce5eb38c755e19ac7cd68ccac1bbc6811) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!203))
- [fix(lan): resolve "Nuevas reglas universales de los monstruos"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@7cdb439101638ef2931e0dab24468de0d530dc88) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!202))

## 1.13.0 (2025-02-27)

### Añadido (6 cambios)

- [feat(lan): objetos maravillosos de la A a la B - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@fb4464e4bbe141566449e2f1406de91cef48b06e)
- [feat(lan): resolve "Bastones - CRB"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@472472f9d44671cebeaee01462c4501a32edda1b) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!199))
- [feat(lan): resolve "Cetros - CRB"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@1783a5343da569ee4919f504a69ad098144a4334) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!198))
- [feat(lan): resolve "Armas mágicas - CRB"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@cf740d5811104c7677bb8eddff71d31fcdb0920b) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!197))
- [feat(lan): resolve "Escudos mágicos - CRB"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@2a0c8b390f34bdd4a11b93a3521a1d5582b85bf9) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!196))
- [feat(lan): resolve "Armaduras mágicas - CRB"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@1c3805d03d8efd7823e78dd36ca70fc3a56ca84b) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!195))

## 1.12.0 (2025-02-21)

### Añadido (1 cambio)

- [feat(lan): resolve "Misterios de oráculo - GJA"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@70dc5ba85108dcd5af53a0bfc4f445d81f7a6216) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!192))

## 1.11.0 (2025-02-14)

### Añadido (5 cambios)

- [feat(lang/es/): traducidas las razas Tiflin y Oréade](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@9cf387bddbf0f918fcaf53a6bc7061eaf60fd141)
- [feat(lan): resolve "Traducir maldiciones del Oráculo (GJA)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@d00d532ac69b93a9428bb5d8a773c12baeaf5523) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!189))
- [feat(lan): resolve "Traducir clase Oráculo (GJA)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@006e39d0328598cd3d2121ae095fcb46fc5b27d1) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!188))
- [feat(lan): resolve "Traducir clase Cazador (ACG)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@67098f1fcef226b03d0bc3916c0be1cb8239b2ce) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!187))
- [feat(lan): resolve "Traducir dotes - Guía del jugador avanzada (GJA)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@c0fe4244e77526f915a4c54e9b341c011da3c982) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!186))

## 1.10.0 (2025-02-07)

### Añadido (5 cambios)

- [feat(lan): resolve "Traducir dotes - Combate definitivo (CD)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@97b230411969cb0a6b87365297fcc5cc2a992682) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!184))
- [feat(lan): resolve "Traducir dotes - GMI"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@4cc2ed6b12ffe6e4b696a0c419952445bb4968e6) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!183))
- [feat(lan): resolve "Conjuros de Sacerdote de guerra de nivel 1"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@9fa7ec7aef96bac6b6458da5c45b73f9c6dd5306) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!181))
- [feat(lan): resolve "Traducir aspectos de Cambiaformas (UW)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@bd7a1cbee4a9eb137dcceda40a87ad8ae679fd0e) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!180))
- [feat(lan): resolve "Traducir maleficios de chamán (ACG)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@d9730058dc0184b77ed6de0a0cc086133772b377) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!179))

### Arreglado (2 cambios)

- [fix(lan): resolve "Ráfaga de viento tiene enlaces rotos"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@b6aaea6f34bdfee73611636abdf3e8f5595eb5ea) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!178))
- [fix(lan): resolve ""Soltura mayor con el escudo" tiene una errata"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@066bffe902dfe7193209c0da4fa3d4eeabd9a433) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!177))

## 1.9.0 (2025-01-26)

### Añadido (7 cambios)

- [feat(lan): resolve "Traducir espíritus de chamán (ACG)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@e6e73b6e7282ebaf170f49350f6b034ca6ebc2d5) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!175))
- [feat(lan): resolve "Traducir bendiciones de Sacerdote de guerra (ACG)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@94c581de2f7ca6595213bc63ae81f3862bc0f5cf) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!174))
- [feat(lan): resolve "Traducir clase Sacerdote de guerra (ACG)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@1af7cf67043f5827ec98e2b4a73e793486d87544) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!173))
- [feat(lan): resolve "Traducir clase Chamán (ACG)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@b9db4aa0c5362093dd9c244f7ed3054ffdc0b6bd) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!172))
- [feat(lan): resolve "Traducir clase Cambiaformas (UW)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@d67b73fc864f2a33410cfe409b16abe674646318) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!170))
- [feat(scripts/pf1e-es.mjs): añadidas todas las fuentes localizadas](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@ea0f11caba7facb7c79392417522789cf15edc7b)
- [feat(lang/es/pf1.feats.json): añadidas todas las dotes - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@b06b41ac7974ffa30303f691750bb7fb2b3640c3)

## 1.8.0 (2025-01-05)

### Añadido (5 cambios)

- [feat(lan): resolve "Traducir habilidades"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@d50f2dd38fa0e02a689d2df5e32fad44c44d5c0d) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!161))
- [feat(lan): resolve "Traducir Trucos maestros (CD)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@cdc6506217835e65fb7933f28ad21f96d73d80d2) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!167))
- [feat(lan): resolve "Traducir Trucos de Ninja (CD)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@52eb26316f515a8327a126ebf0859cc4a45e200d) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!166))
- [feat(lan): resolve "Traducir clase Ninja"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@7e4ea482a5bf9e4aee688eae2337eadb28bfb5f7) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!165))
- [feat(lang): resolve "Traducir clase Samurái (CD)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@22b280959f57f8b4c4abdfbb19b648c46c6f3075) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!164))

### Arreglado (1 cambio)

- [fix(lan): resolve "Revisar los dominios CRB"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@f4ac374791df03157a00dd85c84112e0449d093a) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!169))

### Borrado (1 cambio)

- [feat(lan): resolve "Borrar entradas en inglés"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@fd1fd6875ce5712038911ee5f6b70c255dec9dfe) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!162))

## 1.7.1 (2024-10-30)

### Arreglado (5 cambios)

- [fix(lan): resolve "Flamear no tiene traducida la acción "Hurl""](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@e9a608f4093a030dc6602e47b7233a76acb87684) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!159))
- [fix(lan): resolve "Pirotecnia no tiene las acciones ni las descripciones de estas traducidas"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@48590e397b2a3416385f0bdc3490fe365779b54f) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!158))
- [fix(lan): resolve "Forma gaseosa tiene enlaces rotos"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@9f43710ea5367de94f077e165a68fdb79946ec64)
- [fix(lan): resolve "Embestida mayor está incorrectamente traducida"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@eb82aab47f4584277a041c770a4acc35c54131cd)
- [fix(lan): resolve "Hendedura no tiene traducidas las notas de contexto de la acción"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@2a13e809f12a873682dad4330c76600777d62793)

## 1.7.0 (2024-10-24)

### Añadido (2 cambios)

- [feat(lang/es/pf1.spells.json): algunos conjuros ad-hoc](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@4f011711fbe46e4526bb86ee41a5de36a8ff4d26)
- [feat(lang/es): resolve "Traducir anillos mágicos - CRB"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@6bf78a0647fda3b85999e5ec750ca5e0305df68c) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!152))

## 1.6.1 (2024-10-14)

### Arreglado (1 cambio)

- [fix(lang/en/): traducciones faltantes con v10.8](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@afeb47af5091b9fa0e7009e7d35ed239cb0a8996)

## 1.6.0 (2024-10-13)

### Añadido (1 cambio)

- [feat(lang/es/pf1.spells.json): conjuros de hechicero de 1er nivel](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@91b97587eb7cffbf8997785be1073b5d74ba41f2) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!149))
- [feat(lang/es/pf1.spells.json): conjuros de mago de 1er nivel](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@91b97587eb7cffbf8997785be1073b5d74ba41f2) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!149))
- [feat(lang/es/pf1.spells.json): conjuros de adepto de 1er nivel](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@91b97587eb7cffbf8997785be1073b5d74ba41f2) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!149))
- [feat(lang/es/pf1.spells.json): conjuros de alquimista de 1er nivel](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@91b97587eb7cffbf8997785be1073b5d74ba41f2) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!149))
- feat(lan): creado weblate público para contribuir en las traducciones ([weblate](https://hosted.weblate.org/projects/pathfinder-1e-spanish-compendium/))

### Arreglado (3 cambios)

- [fix(lang/es/pf1.items.json): corregidos algunos objetos](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@372d5f881522990bdafca08e2e501c0dbe597d73)
- [fix(lang/es/pf1.classes.json): quitar alineamiento, riqueza inicial, dg de las clases](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@60587c4c52aeb496af458fda3206bf500aa25478) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!150))
- [fix(lang/es/pf-content.pf-feats.json): errata en dote](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@50ec724d5732343f049c4d0cdec8b3cbc3846350)

## 1.5.3 (2024-09-27)

### Arreglado (1 cambio)

- [fix(lang/es/pf-content.pf-feats.json): traducidas algunas dotes para uso personal](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@2fffd6bc48cb5102fdc287b5ce195f891764c3ef)

## 1.5.2 (2024-09-26)

fix(lang/es/pf-content.pf-wondrous.json): añadidos algunos objetos mágicos para uso personal

## 1.5.1 (2024-09-17)

### Arreglado (3 cambios)

- [fix: resolve "Revivir a los muertos no traduce correctamente el objetivo"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@34132f458a03db7798b4aa651c792635898aec92) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!145))
- [fix: resolve "No se traduce one day/level correctamente en duración"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@464de947ac53e57993e1b302ebdb6152e1339556) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!144))
- [fix: resolve "Detectar el mal tiene una errata"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@e5629c38ee08a9e8044a42a4b64d63568d06a607) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!143))

### <type> (1 cambio)

- [fix: traducir acciones del Escudo pavés](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@140fbf9495b90db6e6d562b26c0b598b875c2e69) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!142))

## 1.5.0 (2024-09-08)

### Añadido (4 cambios)

- [feat(lang/es/pf-content.pf-magicitems.json): añadido Bastón de encantamiento](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@45d86a054531536dffca28e2c653846deb534de4)
- [feat(lang/es/pf1.monster-templates.json): plantilla de Gusano que camina](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@387b7fb08d80a6f9bb2ebfcad6726eb22e0f3920)
- [feat: resolve "Descubrimientos de Alquimista - Combate Definitivo"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@188b94abc0d9e673b83da019d5d7688591057cd2)
- [feat: resolve "Conjuros de paladín de 4º nivel"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@2ab7316beb24839641a93a28097bda80f994fad4) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!120))

### Arreglado (2 cambios)

- [feat: resolve "Enlace roto en Bendición de fervor"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@43d962d21409c8d1719f507f2df01d43d13967b3)
- [feat: resolve "Dormir tiene área parcialmente traducida"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@7a1c9b845bff643725a5cd04961500bcc9366e19)

### Otro (2 cambios)

- [chore(sync): sincronización de archivos con PF-Archetype](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@e2ce70ad981c6a302322a89f2dc0da92cf2450b9)
- [chore(sync): sincronización de archivos con PF-Content](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@c88ca59eb68fcc22c52c5307e08dd81df2cecc44)

## 1.4.1 (2024-08-21)

- Soporte y migración a Pathfinder 1e v10.5

## 1.4.0 (2024-08-16)

### Añadido (7 cambios)

- [Resolve "Traducir Ring of Sustenance"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@f2314140d2bbe97c714d056e4307fffc74e42031) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!132))
- [Resolve "Traducir Bag of Holding tipos de 1 a 4"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@61e23ffc411103bd18308f50f7aeca84c2b3bd11) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!131))
- [Resolve "Proezas - Combate definitivo (CD)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@6cd64792072120bcc7afc0ad4eb4112b4a22694e) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!130))
- [Resolve "Clase pistolero"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@c5378c04fa1dc6933247d60bb23a8e7e7edf6e44) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!127))
- [Resolve "Talentos avanzados de exterminador - Advanced Class Guide (AGC)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@bdc1806988d577c615431530951951ca8c79b9ed) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!129))
- [Resolve "Talentos de exterminador - Advanced Class Guide (ACG)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@23160ebc65b2e227724af6400d74590e62094deb) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!126))
- [Resolve "Clase exterminador"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@dd6b951cb98778aa95b4d3101e59d85816baf84b) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!125))

### Arreglado (6 cambios)

- [fix(lang/es/pf1.weapons-and-ammo.json): borradas estadísticas y descripción](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@ae2b7cb0b1f47bc80e96c06c69635434bf208926)
- [fix(lang/es/pf1.weapons-and-ammo.json): borrado Fuente ya que está en el sistema](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@6b1ca9d466be6ff05aa65f29cd5d0fdedd9e8f65)
- [refactor(lang/es/pf1.classes.json): actualizadas las descripciones de las clases](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@1411dcab4824edd18a2e39bf4baeef24de293585)
- [refactor(lang/es/pf1.class-abilities.json): actualizadas descripciones de habilidades de clase](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@04d4d734f9ad7e17ca3961737754abd90d6e8a7e)
- [refactor(lang/es/pf-content.pf-feats.json): actualizadas las descripciones de PF Content](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@3345c350a4192858ca68d02107959f3873769bdc)
- [Resolve "Caustic Eruption y Spellcrash, Lesser no tienen traducción"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@f51db4eba3399bb7d38439b178a89991def7fc99) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!128))

### Seguridad (1 cambio)

- [feat(module.json): se han ajustado las versiones de module.json](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@2a8eb20f4eb8ebe478f73c554c8a5f6f8701125a)

## 1.3.1 (2024-08-08)

- Algunas traducciones en caliente

## 1.3.0 (2024-08-07)

### Añadido (20 cambios)

- [Resolve "Descubrimientos de Alquimista - Guía del jugador avanzada"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@9a1fbaf585e9debe07718ea217a4689aea616613) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!122))
- [Resolve "Excluir action.name de aquellas que son automáticamente traducidas"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@febcd4861b2703d991c8333f755f8c01f8e638a7) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!121))
- [Resolve "Arcanos de Magus - Magia Definitiva"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@9f223b5ea552793f8dc8f5f2eff86c1746121f57) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!119))
- [Resolve "Arcanos de Magus - Combate Definitivo"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@aa9ae2493aeeecb6d5c6eb901cdf3029799f2ebb) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!118))
- [Resolve "Excluir tipos de documentos no localizables del macro"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@6760b5275ea205a1c9468af10906dc15d93c6b2b) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!117))
- [Resolve "Traducir clase Magus"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@4d8bf0db1c938034824369811fff81507ff3c2c1) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!116))
- [Resolve "Filtro para buscar por nombre original"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@64a7383925e8155a1bc5af44e87dfaa70391ecb7) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!112))
- [Resolve "Traducir Incense of Meditation"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@2ba2535a745029d1128f5dddd1ac8d77705d455a) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!111))
- [Resolve "Traducir Eyes of the Eagle"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@029974bda6a64849b71d9bbbaf229ab19a428ed3) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!110))
- [Resolve "Traducir Belt of Giant Strength de +2 a +6"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@bf6c9550561e50f6f177b5719a54304b2a8576df) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!109))
- [Resolve "Traducir Belt of Mighty Constitution +2 a +6"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@41e9f0946670a8e468a097280d60421b51a1b2d8) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!108))
- [Resolve "Traducir Headband of Alluring Charisma +2 a +6"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@74dba7ac8b15e22cd1dd642fd03303e72e2bb5ca) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!107))
- [Resolve "Traducir Headband of Inspired Wisdom +2 a +6"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@ab577a093bb8faaf9a2fde03525f4c635c453f56) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!106))
- [Resolve "Traducir Headband of Vast Intelligence +2 a +6"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@1fd44655a73140201c58cc6fca4ed329d3bedcba) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!105))
- [Resolve "Macro para extraer paquetes"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@a8b2d1f42b63ca7230f03b3ea28662c1335f98d3) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!103))
- [Resolve "Traducir Braces of Armor y nombres de carpetas"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@4bef87b57cb42371e9ea5bc68ad2d44e75c63c72) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!100))
- [Resolve "Traducir dotes de creación de objetos (CRB)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@47d06c5d185441bc4d5d4d43ee16db82c289da0b) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!98))
- [Resolve "Traducción Ring of Protection +1 a +5"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@4f93d16bcb5c7af63d97670d3ac4214a07289197) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!97))
- [Draft: Resolve "Traducir Cloak of Resistance +1 a +5"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@760171f010945b07c578bea186192263782855d6) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!96))
- [Resolve "Traducir Boots of Striding And Springing"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@409ca1ad21e3cb9b3489bb5730e9b417957bdd01) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!95))

### Arreglado (5 cambios)

- [fix(lang/es/pf1.class-abilities.json): añadida nota de contexto faltante y borrado enlace externo](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@e4ad213d099ec36c32a6ac68231c029e5f980181) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!113))
- [Resolve "Cinturón de destreza increíble no tiene sección de construcción"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@8b173b049cc42a53e30dcb2bdca1ec740a9fdb43) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!104))
- [Resolve "Alarma está parcialmente traducido"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@db7ece948f76be296c43aa3ba34b6b86af436a84) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!102))
- [Resolve "El buff Piel robliza no tiene enlace al conjuro"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@7d7e705f581da4c537422653cd702aa4d499be59) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!101))
- [Resolve "Kit de curandero está mal traducido"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@66dd68bd7e5652d1d119d471daa88a91d2c6e317) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!99))

### Seguridad (1 cambio)

- [build(module.json): cambiada la versión máxima Pathfinder 10.5](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@4275a4ff821c61f6877842ad7a15ed8872e4402c)

### Otro (2 cambios)

- [fix(tools/macros/gettranslationfile.mjs): se ha arreglado el macro de extracción](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@6cbfc354c4ef38597227ed1d7a5382010f52a628)
- [Resolve "Arreglar el linter y añadir comprobaciones para mocha"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@2c5e0a07a9b486b36a52cd8ee5f72a0ce40bcca8) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!114))

## 1.2.1 (2024-07-29)

### Arreglado (1 cambio)

- [fix(scripts): error al leer condicionales de algunos objetos](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@2daa8842b9f32b8925a1cd3862fea25c8ec9e07e) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!93))

## 1.2.0 (2024-07-29)

### Añadido (1 cambio)

- [feat(pf1.spells): todos los conjuros de paladín de 3er nivel](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@b41056b01e8c1ccbf660c479acfa40dcf8114fa3) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!90))

### Arreglado (2 cambios)

- [fix(scripts): ataques extra no se traducían](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@01e7bd7b2273804245a04a85fdef6b082f7ece3a)
- [fix: error de auras no traducidas](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@a3a06f56e48cbbbe620725236430b8f15a864d8f)

### Seguridad (1 cambio)

- [feat(test): añadido Quench como herramienta de pruebas](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@c72912a6d75f4abf934470e179ba512af03407b1)

## 1.1.1 (2024-07-08)

### Arreglado (1 cambio)

- [Resolve "Algunas habilidades de clase compartidas hacen referencia a la clase en la que salen"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@4ffaec4e6073e6f9fcc1f686bcc794d89a0468f5) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!88))

## 1.1.0 (2024-07-06)

### Añadido (6 cambios)

- [Resolve "Traducir las nuevas acciones por defecto"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@6def93749583da9b7d7cdfbd149d9f3ece485660) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!86))
- [Resolve "Tipos de criatura (Bestiario 1)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@f48867f27f71dc179ddbace67b426f4fad8a925a) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!85))
- [feat(pf1.racialhd): clase Cieno](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@79767dad73955aae41e971bf0e311fd2553dde7e)
- [feat: los objetos dentro de los contenderes ahora están traducidos](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@83381fb24ae5e7b6bd4e4e715d0ed42efc1c67aa)
- [feat(pfc-umr): entrap traducido](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@a83643e42f94ea51e747677ea604e605cd6c7196)
- [feat(pfc-umr): freeze, split y ooze traits traducidos](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@5f0817659d7f67b3d3cd9d06b0ccc7db2e2aca10) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!84))

### Arreglado (1 cambio)

- [fix(pfc-umr): enlace roto de Ceguera ante la luz](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@68e3042da5440052f9eed83a79303392090d03fb)

### Cambio (1 cambio)

- [refactor(logger): mejor output de errores en las traducciones](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@6ead2b08f92f37dbc0e993b8ca3258a5bad48c4b)

### Borrado (1 cambio)

- [refactor: uso de un nombre adecuado de fichero](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@c8a1231891d880e7fc433cb30eba62b836f8c215)

## 1.0.1 (2024-06-25)

Sin cambios.

## 1.0.0 (2024-06-25)

### Añadido (1 cambio)

- [feat(class-abilities): traducidas nuevas habilidades de clase - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@9261d00f402e40d2144665543ae6d320df7bd977)

### Arreglado (1 cambio)

- [fix(scripts): error en el script de despliegue de notas de contexto](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@f7b4a460cb731125a5febfdfcbbd7ede966a8495)

### Obsoleto (1 cambio)

- [feat: migración a Pathfinder v10](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@a53c56242e31ec5ca8f843a2d7e3c23cc9000c64)

## 0.10.0 (2024-05-23)

### Añadido (11 cambios)

- [feat(pf1.spells): conjuros de clérigo de 9º nivel - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@4701d3dad0fc080bf3e449e62860d8e1367d5cd5)
- [feat(pf1.spells): conjuros de clérigo de 8º nivel - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@57d1567642ce34df6d151d069e131758cd0f18aa)
- [feat(pf1.spells): conjuros de clérigo de 7º nivel - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@59787e702cd3fc5b037ff24aedb9392634d6ab36)
- [feat(pf1.spells): conjuros de clérigo de 6º nivel - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@fa04e64c3b9044b56461b5f84acd00a550a5a8ea)
- [feat(pf1.spells): conjuros de clérigo de 5º nivel - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@e0b6a2524a8b758c8f717489873ff8a65e5eeb29)
- [feat(pf1.spells): conjuros de clérigo de 4º nivel - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@1f479ecf2016ebd2dfd2c3fe0b972b312b48d6f1)
- [feat(pf1.spells): conjuros de clérigo de 3er nivel - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@05db6858fbe4b275cc8963f4c151bb3e2ca32ac3)
- [feat(pf1.spells): conjuros de clérigo de 2º nivel - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@e0db96885ba4a43720e6544c8ea8e3b25fe4a11e)
- [feat(pf1.spells): conjuros de clérigo de 1er nivel - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@5c822006285220b07b1e01f5e6fa0972aefcd32d)
- [feat(pf1.spells): conjuros de bardo de 5º nivel - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@91becd102e41ff9a28a61d777d8a3a0457a79d2b)
- [feat(pf1.spells): conjuros de bardo de 4º nivel - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@13913cd4f52b5b3de13bf953123364e56066d516)

## 0.9.0 (2024-05-08)

### Añadido (8 cambios)

- [ci(versioning): automatización de changelogs](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@2816182f55afddf18753cda10ec7981477cc830f) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!73))
- [feat(pf1.spells): todos los conjuros de bardo de nivel 0](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@4e7cb018bc7afdb922260a4f13a3f96f7212f4ad)
- [fix(tools): fin de soporte para Windows](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@baa0896a5b4f262c7a7f86018cc5490a7b8db1bf)
- [feat(pf1.spells): conjuros de bardo de 3er nivel - CRB](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@7e1d3e6893f7a1c49b8016a8ce4616c37791d4e1)
- [feat(pf1.spells): conjuros de bardo de nivel 2 (CRB)](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@0ce90a5942675bbde43a5a5bbbdbbe0f50f8c159)
- [feat(pf1.spells): conjuros de bardo de nivel 1 (CRB)](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@f935fdf8b00c529d4a0924b92b62dcb68d97679e)
- [feat(pf1.spells.json): conjuros de bardo de nivel 0](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@1c4f9aaecf6e863471f3266f9d70e0659fcb7056)
- [feat(class-abilities): resolve "Maleficios de Bruja (GJA)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@5ea5f20375cb69e692393f86a8418d7036934049) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!71))

## 0.8.1 (2024-03-04)

### Añadido (1 cambio)

- [fix: traducido arquetipo 'Maestro del cuchillo' ad-hoc](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@f1e82158780212cf7c605606970514124c411dca) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!69))

## 0.8.0 (2024-03-02)

### Añadido (6 cambios)

- [feat(lan): resolve "Talentos avanzados de pícaro (CRB)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@429bc4dd308eae827beb0e712d1380743d3acbcc) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!67))
- [feat(lan): resolve "Talentos de pícaro (CRB)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@80564a6309cbbdd96f93b1b5436303bb7f906e8e) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!66))
- [feat(lan): resolve "Clase Pícaro (CRB)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@337be13e79df9be30c9ce6c4a5abfa81295d538b) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!56))
- [ci(gitlab-ci): añadido trabajo 'commit-lint' para asegurar buena estructura en cambios](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@14c1930ca06482a7132dfa3998ee8c9e21b66033) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!65))
- [ci(feat): resolve "Automatización de despliegues versionados"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@7aefb4e0fdfa2d062e73bb38e0fd63ec9b5651da) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!60))
- [feat(devtools): integración con semantic-release](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@fd660c53479ad41040b8d34785453260e6638313)

### Optimización (2 cambios)

- [ci(feat): resolve "Integrar automatización de versionados"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@e716bf07a73228872b4fb8be16896bc6ddff7ebb) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!62))
- [ci(automation): resolve "Integrar herramienta de versionado"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@3de6b79742b8b9746f9425e440069183933219df) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!57))

## 0.7.4 (2024-02-19)

### Añadido (2 cambios)

- [feat(lan): dotes de los monstruos (B1)](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@0eea6f5d969a648a06e9bd5b16443edc8409e03f) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!47))
- [fix(lan): corregidas las fuentes de las clases](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@abbe977ce0935473511dd6ee09a22dd19540dfad)

### Arreglado (1 cambio)

- [fix(lan): borrado carácter '\n'](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@8165b68d5069d315862ef1c8dba777bd261c648b)

## 0.7.3 (2024-02-19)

### Añadido (1 cambio)

- [feat(devtools): generación automática de la tabla](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@1abeef438e4a106f936f752eca474ecf5ecf52b6)

### Arreglado (1 cambio)

- [fix(lan): clave incorrecta en Spell-Like Abilities](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@5b34b64c4c015e96bf37734353a442557b7ba96c)

## 0.7.2 (2024-02-15)

### Añadido (3 cambios)

- [feat(lan): traducidos estados](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@45b9b68916efae7a1769201594d78b66facbb685)
- [feat(lan): algunos conjuros de druida](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@3a78317a231687036beb29db93640f344b12db45)
- [feat(lan): dotes sueltas](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@aad190ddf79226bc1ad23b26513eb70cf45ef863)

## 0.7.1 (2024-02-09)

### Añadido (4 cambios)

- [feat(lan): traducido Samsarano](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@6f909f7c836abf84742f3c5938e418cbf55de654)
- [feat(lan): traducido Telecinesis](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@b91c52b5995228d2635cee6b2501a20f20c20ba9)
- [Resolve "Reglas universales de los monstruos (Bestiario 1)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@23b345b22edbc1653ee4808c7e3b4e839a3d11bb) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!40))
- [Resolve "Clase Monje"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@b2c0b005f4b43d595621cd20a7efb305c477b9e1) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!45))

## 0.7.0 (2024-02-03)

### Añadido (7 cambios)

- [Resolve "Linajes de Hechicero (CRB)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@b9bca6f102df6008b1a1a5518b52be36628a97f4) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!44))
- [Resolve "Clase Hechicero"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@7c227c623143def879d7645727765fa2fc70a73c) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!43))
- [feat(lan): est. arma a dos manos y dotes asoc.](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@560b3f8398325149d75aa0463a7fbb09872477cf)
- [Resolve "Dotes de Combate (CRB)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@29beae5086c44a724d274a5a721ab38d91f7e13b) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!39))
- [Resolve "Clase Guerrero"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@5f353795afd9dd4464367c6c2f1cdebd59e76aaf) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!38))
- [feat(lan): localización de fuentes para razas](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@4606cb767f94069990ade8ddedf7e492ae469870)
- [Resolve "Clase Explorador"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@525991fdc6af9815aa15fbc184f44155b6720604) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!37))

### Arreglado (3 cambios)

- [Resolve "Saco de dormir no está traducido"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@bc3eb67194cc4a0479a052af6a151fe669ca576d) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!42))
- [Resolve "Notas de contexto no traducidas en las clases CRB"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@d48515209136d6c95cd119b127426185a4abc266) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!41))
- [fix(lan): errata al traducir "Blades, Light/Heavy"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@48f303cb82c199082a411ed04fe7e4e29b66d482)

## 0.6.0 (2024-01-14)

### Añadido (11 cambios)

- [Resolve "Dominios de Clérigo (CRB & GMI)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@2db0904bb74e9757353e30c11d0bc67a482ebb70) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!34))
- [Resolve "Clase Clérigo"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@c70ab138f80a8a691cc84ab91be1e24d993dc2c7) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!33))
- [Resolve "Interpretaciones de Bardo (CRB)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@3a9ecf18862357f5948a26b6f967563b625be353) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!32))
- [Resolve "Clase Bardo"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@56f296f4e0b0c5fb0139045e3b9dcc3547527225) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!31))
- [Resolve "Poderes de furia del Bárbaro (CRB)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@445e4785b5645f9dfa2094feccaf5bbf9999a7cc) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!30))
- [Resolve "Clase Bárbaro"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@1c0f3f49af70bb2d0592fbff1edb13294bdf273f) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!29))
- [Resolve "Clase Druida"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@23983c0bb85f8b669844433b723a6ab4deae94d9) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!28))
- [Resolve "Buffs de PFContent"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@200d9579c30751a7bdf0623a2da4e487a3d28e10) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!24))
- [feat(lan): conjuros asociados a objetos](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@c7d87e84f3fbdef476ac562b2bfbe61874694da0)
- [Resolve "Traducir Objetos Core Rulebook"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@1bf2962be1c86b26a712c8084726a2ac5bf2258d) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!18))
- [feat(lan): traducidos algunos talentos de pícaro](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@a760b08458f8b6a79c98e92adbf4fd9ed7e81111)
- [Resolve "Aptitudes de compañeros animal (CRB)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@7d6e355519a2e8bdb043c1e563df7eb9cd30e6aa) [merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!35)

### Arreglado (2 cambios)

- [Resolve "Escuela universalista no tiene enlace a los poderes de escuela"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@b1bee50e5d4d7e3ec8babbf169c692aa8371be54) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!36))
- [Resolve "Faltan traducciones en el conjuro Dormir"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@df10a3096dbeba5e42e50e74617a97c2e3f24ca2) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!27))

### Seguridad (1 cambio)

- [feat(dev): esquema de JSON de traducciones](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@eacd0e3531879c529054425812c8bef7b07b20e3)

### Optimización (1 cambio)

- [feat(tool): herramienta de combinación de JSON](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@eb9c5a97e23435a1483efe5884ca4e102d2cf4b7)

## 0.5.11 (2023-12-08)

### Añadido (2 cambios)

- [feat(lan): traducidas algunas evoluciones de eidolón](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@510c594195a556c7014dfd9d10a71766ff9ef55a)
- [feat(lan): traducidas algunas dotes de combate](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@a4331f4c527c63156fedf4f0b4428d151668b15a)

## 0.5.10 (2023-12-06)

### Añadido (3 cambios)

- [feat(lan): traducido el sistema de fuentes](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@fa492a87b15de1686d51323767a4ca190a815b42)
- [feat(lan): traducida la clase Convocador](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@aedb7389b378cdde5e18a721a01a29adb217fd6b)
- [feat(lan): traducida la clase Alquimista](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@1a7c2274e72c5b4b42530e54614d0e5adf7cde2a)

### Arreglado (3 cambios)

- [Resolve "El buff Piel robliza no tiene enlace al conjuro"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@dd168f87eb81b854eaea8ecfdd81919fc4433594) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!26))
- [Resolve "Falta traducción de Efecto en Aliado de los planos menor"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@156102f7bc82d082713dfe59e2059690ce2c2453) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!25))
- [fix(doc): cambiada la URL tras el traspaso](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@11e3afbe1506a7ab3314247d8e31451b0359e923)

## 0.5.9 (2023-11-26)

### Añadido (1 cambio)

- [feat(lan): traducidas algunas dotes en caliente](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@0da2efac7854c2e978703f556767db0a3f7ee9d7)

### Arreglado (2 cambios)

- [fix(doc): añadida fila "Armaduras y escudos"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@b72d4189cfbf87f673b1dc569e84e2110df6c15f)
- [Resolve "Algunos Buffs Comunes no tienen traducidas las Notas de Contexto"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@ddb76cd57285e732bc402d288d03ac6f3af6ed47) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!23))

## 0.5.8 (2023-11-25)

### Añadido (1 cambio)

- [feat(lan): traducciones variadas y arreglo Deseo](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@fa0085006ce6fb5d5f1d713a4df36eda00dcf93d)

### Arreglado (2 cambios)

- [Resolve "Conjuro "Ira del orden" sin traducción de Área"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@da60bc45bafc34b1833ad53e3fc2b35e87e456ee) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!22))
- [Resolve "Componente faltante de traducción en Volar"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@45c1ea63db3995017a8096b436c73989af3e10bb) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!21))

## 0.5.7 (2023-11-22)

### Añadido (1 cambio)

- [feat(lan): algunas traducciones de varias categorías](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@8fcaa08d442117a1c77e24409c6cc45df37edd51)

### Arreglado (2 cambios)

- [Resolve "Actualizar la traducción de Furia del Bárbaro"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@0f216f787025555d567ae22e6b5f15ddcb293822) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!20))
- [Resolve "Cambiar traducciones de Toque de idiotez"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@b94019a7544896473b15448f88181964f75cd3ce) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!19))

## 0.5.6 (2023-11-20)

### Añadido (2 cambios)

- [feat(lan): traducidos algunos conjuros en caliente](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@2279d95665cb6d485b4c5570fc2778ade2416d52)
- [feat(lan): traducidas las razas de Core Rulebook](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@b18e28633fce927e03639f91f000804b91fbcacd) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!17))

## 0.5.5 (2023-11-17)

### Añadido (1 cambio)

- [feat(lan): traducidos conjuros de paladín 2](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@9594dc8fa5b602f9989a070c081329be852f4c31) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!16))

### Arreglado (1 cambio)

- [fix(lan): error al traducir habilidades de clase](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@ff9bc63ce2e80746bdc194ad8590b6bf3bba82f7)

### Seguridad (1 cambio)

- [test(lan): tests para traducciones de acciones](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@ace1c413e2ea338e373dbc26b9fc7f044ee8eb8a)

## 0.5.4 (2023-11-15)

### Añadido (1 cambio)

- [Resolve "Clase Paladín"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@00cfc5b889bfabb33afba5eadfa8a2fadf6f4c71) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!15))

### Arreglado (1 cambio)

- [fix(lan): error al traducir habilidades de clase](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@ff9bc63ce2e80746bdc194ad8590b6bf3bba82f7)

## 0.5.3 (2023-11-02)

### Añadido (3 cambios)

- [feat(lan): traducidos conjuros de paladín 1](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@cfb6f14737dccdcde0b6ce90cf8dd8ffa17b7a5c)
- [feat(doc): añadida una tabla de traducciones](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@e1708e69d71d7d12fd14b9aca84491fe235546eb)
- [feat(tools): herramienta porcentual traducciones](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@c86da1cfb636348346f649ade9b86a142ae82e99)

### Arreglado (3 cambios)

- [Resolve "Errata en Armas y munición (Espada ancha de nuevo anillas)"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@73f802306d6ed5f58a61784a8df016a44c4d5b4e) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!14)) ([issue](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es#21))
- [Resolve "Errata "Poliformar funesto""](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@d329357a641fcbec0d9ce083f30700069536a05f) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!13)) ([issue](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es#23))
- [Resolve "Enlace roto "Soltura mayor con los conjuros""](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@2d48edc4b6f5b700fed6e2260ba064724837fd0b) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!12)) ([issue](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es#24))

## 0.5.2 (2023-10-26)

### Añadido (5 cambios)

- [feat(lan): traducidas algunas habilidades de clase](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@45c2090ae5e127b4478303d663695590c1189d0d)
- [feat(lan): traducidos algunos conjuros](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@72f768f35a73a06e5b5c1cd12ebe1f9724ed4026)
- [feat(lan): todos los conjuros Curar heridas](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@12e4a5007a55ed45d1e8c2e91c11a0d10b7b8cb9)
- [feat(lan): traducidas trucos de bruja](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@cb23da6bb958cdc0a89184389469002052fb0b01)
- [feat(lan): traducida bruja hasta nivel 10](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@539d23709338cd13aa51bb894780716a2f5d2101)

### Arreglado (1 cambio)

- [fix(lan): error en múltiples salvaciones](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@7b68dbb35012b4876ce6a1dab9f123301fc5ee43)

## 0.5.1 (2023-10-23)

### Añadido (1 cambio)

- [feat(tools): añadidas herramientas de desarrollo](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@3f81b92a211b3d3eb6c81f0fbb8e8374787c5f5f)
- [fix(lan): traducidas escuelas de magia](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@211ee4debc33dc8a7b2e8117a042f011775b5fda) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!10)) ([issue](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es#7))

## 0.5.0 (2023-10-17)

### Añadido (7 cambios)

- [feat(lan): traducidas armas de asedio](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@81c833d1ccb9b47725f5a4e6011766d36ab68458)
- [feat(lan): traducidas armas de la edad de piedra](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@aa0d0e0018afc2b3f0349724882a2fd06640b626)
- [feat(lan): traducidas armas orientales](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@f84f0938a8564fb4cf3008c4c0b99d3c050fddbb)
- [feat(lan): traducidas armas de gladiador](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@f4f4f65b4745862101803a171ee23caf2bea7243)
- [feat(lan): traducidas armas de fuego](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@977edf811dd194f63be37c94a65e489edf1aec0e)
- [feat(lan): traducidos linajes](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@2d266d3f84d7d5e9e184f429e15f532bec60fa23)
- [feat(lan): traducidos dominios y subdominios](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@d482c50a6bb1320d78d8c31849664f7c026ddde5)

## 0.4.5 (2023-10-15)

### Añadido (2 cambios)

- [feat(lan): traducido buff "Volar"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@5821a0d54db92acbe78d5c5e7d2cbb7a474027d5)
- [feat(lan): traducidas algunas dotes muy usadas](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@d163855b21553c44bebdb0cea019f34fb048e96f)

## 0.4.4 (2023-10-14)

### Arreglado (1 cambio)

- [fix(lan): notas de contexto faltantes](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@6bd05cd601dbf0cdb817b14dd68b40cceab665cc) ([merge request](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es!9))

## 0.4.3 (2023-10-14)

### Añadido (2 cambios)

- [feat(lan): traducidos algunos buffs](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@cdfef4f23cb59258670afe5e9120850b96977106)
- [feat(lan): traducidas algunas dotes](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@f75fa5e46b4e908c15afd8b9b44909612215b0f7)

### Arreglado (1 cambio)

- [fix(lan): error en el separador `or`](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@e3d4186dd7f5b2b82e65fa475aae598297af13e2)

## 0.4.2 (2023-10-14)

### Arreglado (5 cambios)

- [fix(lan): traducida espada larga](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@f7fa35589c8c201ec2b9e41e5deadae2015e834c)
- [fix(lan): armas faltantes (Reglas Básicas)](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@41434dc99225adc326c9f365a32ffe37bdf3c160)
- [fix(lan): errata en custodia vital](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@ce12cab4c3ab10b413c083b9776f78f846f83992)
- [fix(lan): errata "Escudo pesado de metal"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@0282928f89c3a0c33f01fa232d8b70301a604a21)
- [fix(doc): tachado  "Armaduras y escudos" del TODO](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@1bebfb455792324e6c791b81eae362e12d24b17c)

## 0.4.1 (2023-10-14)

### Añadido (4 cambios)

- [feat(lan): armas y municiones (GJA)](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@8b04eaa857367b9dbe8b63b5e2dcdfd1077b4706)
- [feat(lan): poderes de furia (GJA)](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@34dfeada6aeee26e7b7140f1196b1c8b71d5c622)
- [feat(lan): poderes de bárbaro (Reglas Básicas)](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@70f678d48a57e2659d42c99fbd5dd89881b42c04)
- [feat(lan): habilidades de clase para el bárbaro](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@a42b83b30043c95c1c914703f1aae548ae1d97c9)

### Arreglado (2 cambios)

- [fix(lan): errata "Escudo pesado de metal"](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@0282928f89c3a0c33f01fa232d8b70301a604a21) ([issue](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es#1))
- [fix(doc): tachado  "Armaduras y escudos" del TODO](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@1bebfb455792324e6c791b81eae362e12d24b17c)

## 0.4.0 (2023-10-12)

### Añadido (1 cambio)

- [feat(lan): traducidos los buffs comunes](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@6391ee9066b8f235eadaf6345d8a0e23c83de04b)

## 0.3.4 (2023-10-11)

### Añadido (3 cambios)

- [doc(changelog): añadido `CHANGELOG` automático](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@631c3feba954250f2d983fc64bbbf8c5f905e3f6)
- [feat(lan): traducidas armaduras pesadas](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@6561eeb6e0ad2f6a5930613285822880bf7f8caf)
- [feat(lan): traducidas armaduras intermedias](lecuay-fvtt/pathfinder-1e/foundryvtt-pf1e-es@637196130d67d6ca5c74bb0e2f17d6d2b1d1c898)
