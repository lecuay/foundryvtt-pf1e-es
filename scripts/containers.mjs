/**
 * Translates the items in a container if they have translation.
 *
 * @param {Record<string, Object<string, any>>} items - The items within a container.
 * @returns
 */
export const tContainerItems = (items) => {
  Object.values(items).forEach((item) => {
    /**
     * The original UUID of the item from flags.
     * @type {string | undefined}
     */
    const originalUUID = item.flags?.core?.sourceId;
    if (!originalUUID) return;

    // Retrieve the original item from the compendium
    /** @type {pf1.documents.item.ItemContainerPF | null} */
    const compendiumItem = fromUuidSync(originalUUID);
    if (!compendiumItem) return;

    if (compendiumItem && compendiumItem.flags?.babele?.translated) {
      item.name = compendiumItem.name;
      if (compendiumItem.system) {
        item.system.description.value = compendiumItem.system.description.value;
      }
    }
  });
  return items;
};
