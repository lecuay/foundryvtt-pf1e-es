/**
 * @typedef {object} InputObject
 * @property {string} key - The key to retrieve choices.
 * @property {string} [label] - The label to display (can be localized).
 * @property {boolean} [active] - Whether this choice is currently active.
 * @property {string} [value] - The value of the choice.
 * @property {string} [placeholder] - The placeholder text for the input.
 * @property {string} [type="text"] - The HTML type property of input.
 */

/**
 * @typedef {object} FilterChoice
 * @property {string} label - The label for this choice visible to the user; will be localized
 * @property {string} key - The key for this choice used to identify it
 * @property {boolean} [active] - Whether this choice is currently active
 */

/**
 * Base class for filters that require text input.
 */
class TextFilter extends pf1.applications.compendiumBrowser.filters.BaseFilter {
  static TEMPLATE = "modules/pf1e-es/templates/text-filter.hbs";

  /**
   * List of inputs for this filter.
   *
   * @type {Array<InputObject>}
   */
  static inputs = [];

  /** @inheritdoc */
  hasChoices(number = 1) {
    // We need to allow at least one choice
    if (this.choices?.size == 1) return true;
    return super.hasChoices(number);
  }

  /** @inheritdoc */
  prepareChoices() {
    this.choices = new foundry.utils.Collection(
      this.constructor.inputs.map((input) => [
        input.key,
        {
          ...input,
          label: input.label ? game.i18n.localize(input.label) : undefined,
          placeholder: input.placeholder
            ? game.i18n.localize(input.placeholder)
            : undefined,
          type: input.type ?? "text",
        },
      ])
    );
  }

  /** @inheritdoc */
  reset() {
    this.choices.forEach((choice) => {
      choice.value = null;
      choice.active = false;
    });
  }

  /** @inheritDoc */
  activateListeners(html) {
    html.addEventListener("change", (event) => {
      const input = event.target;
      const value = input.value;
      const key = input.name.split("choice.").pop();
      const choice = this.choices.get(key);
      if (choice) {
        choice.value = this._parseInput(value, choice);
        choice.active = Boolean(value);
      }
      this.compendiumBrowser.render();
    });
  }

  /**
   * Parse the input before it is stored in the choice value.
   *
   * @param {string} textInput - The text input to parse.
   * @param {FilterChoice} choice - The choice object for custom handle by choice.
   * @returns {string} - The parsed value if any transformation, otherwise the original string.
   */
  // eslint-disable-next-line no-unused-vars
  _parseInput(textInput, choice) {
    return textInput;
  }
}

export class OriginalNameFilter extends TextFilter {
  static label = "Nombre original";

  /** @inheritdoc */
  static inputs = [
    {
      key: "originalName",
      placeholder: "Buscar por nombre original en inglés",
    },
  ];

  /** @inheritdoc */
  static indexField = "flags.babele.originalName";

  /** @inheritDoc */
  applyFilter(entry) {
    /** @type {string | undefined} */
    const value =
      foundry.utils.getProperty(entry, this.constructor.indexField) ||
      foundry.utils.getProperty(entry, "name");
    const originalName = this.choices.get("originalName").value ?? "";
    return value?.toLowerCase().includes(originalName.toLowerCase());
  }
}
