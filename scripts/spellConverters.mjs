import {
  tTarget,
  tArea,
  tSavingThrowType,
  tDuration,
  tEffect,
} from "./common.mjs";

/**
 * Gets the spell area are by automation.
 *
 * @param {import("@pf1").documents.item.ItemSpellPF} spellAction Current action to be translated
 */
export const tSpellArea = (spellAction) => {
  let spellArea = spellAction.area;
  spellArea = tArea(spellArea);
  spellAction.area = spellArea;
};

/**
 * Gets the spell effect by automation.
 *
 * @param {string} spellEffect The original spellEffect in english.
 * @returns The automatically translated one.
 */
export const tSpellEffect = (spellEffect) => {
  return tEffect(spellEffect);
};

/**
 * Gets the spell save by automation
 *
 * @param {import("@pf1Docs").item.ItemSpellPF} spellAction Current action to be translated
 */
export const tSpellSave = (spellAction) => {
  let spellSave = spellAction.save;
  spellSave.description = tSavingThrowType(spellSave.description);
  spellAction.save.description = spellSave.description;
};

/**
 * Gets the spell target by automation.
 *
 * @param {import("@pf1Docs").item.ItemSpellPF} spellAction Current action to be translated.
 */
export const tSpellTarget = (spellAction) => {
  let spellTarget = spellAction.target;
  spellTarget.value = tTarget(spellTarget.value);
  spellAction.target.value = spellTarget.value;
};

/**
 * Gets the spell duration by automation.
 *
 * @param {import("@pf1Docs").item.ItemSpellPF} spellAction Current action to be translated.
 */
export const tSpellDuration = (spellAction) => {
  let spellDuration = spellAction?.duration;
  if (spellDuration?.value) {
    spellDuration.value = tDuration(spellDuration.value);
    spellAction.duration.value = spellDuration.value;
  }
};
