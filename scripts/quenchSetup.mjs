import { tContainerItems } from "./containers.mjs";

/**
 *
 * @param {import("@ethaks/fvtt-quench").Quench} quench
 */
export const registerTests = (quench) => {
  quench.registerBatch(
    "pf1e-es.translations.containers",
    (context) => {
      const { describe, it, expect } = context;

      describe("tContainerItems Suite", () => {
        it("The amount of items in the monks kit is the same as returned", async () => {
          /** @type {pf1.documents.item.ItemContainerPF} */
          const monksKit = await fromUuid(
            "Compendium.pf1.items.Item.ergfTqkBYmNCaOKx"
          );
          const items = tContainerItems(monksKit.items);

          expect(items).to.have.lengthOf(monksKit.items.size);
        });
      });
    },
    { displayName: "PF1 Spanish Compendium - Containers" }
  );
};
