import { logger } from "./utils.mjs";

const FEET_TO_METERS = new Map([
  ["20", "6"],
  ["30", "9"],
  ["40", "12"],
  ["50", "15"],
  ["60", "18"],
  ["120", "36"],
  ["180", "54"],
  ["400", "120"],
]);
const MILE_TO_KM = new Map();
const SQ_FEET_TO_SQ_METERS = new Map([["5", "0.46"]]);
const CU_FEET_TO_CU_METERS = new Map([["10", "0.28"]]);

const POUNDS_TO_KG = new Map([["100", "45"]]);

/**
 * Converts feet distance into meters.
 *
 * @param {string} feet The distance in feet to be converted
 * @returns {string} The meters distance converted
 */
export const feetToMeters = (feet) => {
  if (FEET_TO_METERS.has(feet)) {
    return FEET_TO_METERS.get(feet).replace(".", ",");
  }
  if (feet instanceof String) {
    feet = Number(feet);
  }
  return (feet * 0.3048).toFixed(1).replace(".", ",").replace(",0", "");
};

/**
 * Converts miles distance into km.
 *
 * @param {string} miles The distance in miles to be converted
 * @returns {string} The km distance converted
 */
export const mileToKm = (miles) => {
  if (MILE_TO_KM.has(miles)) {
    return MILE_TO_KM.get(miles).replace(".", ",");
  }
  if (miles instanceof String) {
    miles = Number(miles);
  }
  return (miles * 1.60934).toFixed(1).replace(".", ",").replace(",0", "");
};

export const sqFeetToSQMeters = (feet) => {
  if (SQ_FEET_TO_SQ_METERS.has(feet)) {
    return SQ_FEET_TO_SQ_METERS.get(feet).replace(".", ",");
  }
  if (feet instanceof String) {
    feet = Number(feet);
  }
  return (feet * 0.092903).toFixed(1).replace(".", ",").replace(",0", "");
};

export const cuFeetToCuMeters = (feet) => {
  if (CU_FEET_TO_CU_METERS.has(feet)) {
    return CU_FEET_TO_CU_METERS.get(feet).replace(".", ",");
  }
  if (feet instanceof String) {
    feet = Number(feet);
  }
  return (feet * 0.0283168).toFixed(2).replace(".", ",").replace(",0", "");
};

export const poundToKg = (pounds) => {
  if (POUNDS_TO_KG.has(pounds)) {
    return POUNDS_TO_KG.get(pounds);
  }
  if (pounds instanceof String) {
    pounds = Number(pounds);
  }
  return (pounds * 0.4535).toFixed(1).replace(".", ",").replace(",0", "");
};

/**
 * Since for using abilities, weapons, feats... You have either "Attack"
 * or "Use" we can easily translate that.
 *
 * @param {string} name The name of the action (usually Attack or Use)
 * @returns {string} The translated name.
 */
export const tActionName = (name) => {
  const attackNameMap = new Map([
    [/Bash/g, "Golpear con el escudo"],
    [/Ray/g, "Rayo"],
    [/Cone/g, "Cono"],
    [/Line/g, "Línea"],
    [/^Radius/g, "Radio"],
    [/Square/g, "Área"],
    [/Object [tT]ouched/g, "Objeto tocado"],
    [/Bite/g, "Mordisco"],
    [/Throw/g, "Lanzar"],
    // FIXME: This needs to follow the `_handlePlural` convention. Review.
    [/Creatures/g, "Criaturas"],
    [/Creature/g, "Criatura"],
    [/Ignite/g, "Encender"],
    [/Extinguish/g, "Extinguir"],
    [/Heal/g, "Curar"],
    [/Harm [Uu]ndead/g, "Dañar muertos vivientes"],
  ]);

  // Check if the name is already translated
  if (Object.keys(game.i18n.translations.PF1).includes(name)) {
    return game.i18n.localize(`PF1.${name}`);
  }

  attackNameMap.forEach((translation, original) => {
    name = name.replace(original, translation);
  });

  name = _handleDistanceTranslations(name);

  return name;
};

/**
 * Translates the saving throw text.
 *
 * @param {string} saving The saving as such. e.g. "Fortitude halves damage"
 * @returns {string} The translated saving throw.
 */
export const tSavingThrowType = (saving) => {
  const saveTypes = new Map([
    [/Will/g, "Voluntad"],
    [/Reflex/g, "Reflejos"],
    [/Fortitude/g, "Fortaleza"],
    [/negates/g, "niega"],
    [/halves damage/g, "reduce el daño a la mitad"],
    [/halves splash damage/g, "reduce el daño de salpicadura a la mitad"],
    [/(half|halves)/g, "mitad"],
    [/partial/g, "parcial"],
    [/disbelief/g, "descree"],
    [/disbelieves/g, "descree"],
    [/see text/g, "ver texto"],
    [/see below/g, "ver más abajo"],
    [/harmless/g, "inofensivo"],
    [/special/g, "especial"],
    [/object/g, "objeto"],
    [/none/g, "ninguna"],
    [/varies/g, "varía"],
    [/if interacted with/g, "si se interactúa con el conjuro"],
    [/splash damage/g, "daño de salpicadura"],
  ]);
  saveTypes.forEach((translation, originalRegex) => {
    if (originalRegex.test(saving)) {
      saving = saving.replace(originalRegex, translation);
    }
  });

  saving = handleCoordinatingConjunction(saving);

  return saving;
};

/**
 * Translates the target.
 *
 * @param {string} target The target text to be translated. e.g. "one creature or until discharge"
 * @returns {string} The translated target.
 */
export const tTarget = (target) => {
  const commonTargetsMap = new Map([
    [/one creature touched/, "una criatura tocada"],
    [/([oO]ne|a) creature/, "una criatura"],
    [/one touched creature/, "una criatura tocada"],
    [/(one|a) undead creature/, "una criatura muerta viviente"],
    [/one other touched creature/, "otra criatura tocada"],
    [/all creatures/, "todas las criaturas"],
    [/(one|a) touched object/, "un objeto tocado"],
    [/one humanoid creature/, "una criatura humanoide"],
    [/one living creature/, "una criatura viva"],
    [/one or more living creatures/, "una o más criaturas vivas"],
    [/[oO]ne or more creatures/, "una o más criaturas"],
    [/one dead creature/, "una criatura muerta"],
    [/one incorporeal creature/, "una criatura incorpórea"],
    [/one corporeal creature/, "una criatura corpórea"],
    [/one additional creature/, "una criatura adicional"],
    [
      /one additional willing creature touched/,
      "una criatura voluntaria adicional tocada",
    ],
    [
      /other touched willing creature(?<Plural>s)?/,
      "otra{PLURAL} criatura{PLURAL} voluntaria{PLURAL} tocada{PLURAL}",
    ],
    [
      /willing corporeal creature(?<Plural>s)? touched/,
      "criatura{PLURAL} corporal{PLURAL} voluntaria{PLURAL} tocada{PLURAL}",
    ],
    [/one additional willing creature/, "una criatura voluntaria adicional"],
    [/one willing creature touched/, "una criatura voluntaria tocada"],
    [/one willing creature/, "una criatura voluntaria"],
    [/one willing living creature/, "una criatura viva voluntaria"],
    [/one good-aligned creature/, "una criatura alineada con el bien"],
    [/one crystalline creature/, "una criatura cristalina"],
    [/one extraplanar creature/, "una criatura extraplanaria"],
    [/one indifferent or friendly animal/, "un animal indiferente o amistoso"],
    [/one solid object/, "un objeto sólido"],
    [/one weapon/, "un arma"],
    [/one melee weapon/, "un arma cuerpo a cuerpo"],
    [/one elemental/, "un elemental"],
    [/one eidolon/, "un eidolón"],
    [/one animal/, "un animal"],
    [/(one|a) object/, "un objeto"],
    [/(one|a) construct/, "un constructo"],
    [/[sS]elf/, "tú mismo"],
    [/several living creatures/, "varias criaturas vivas"],
    [/allies within range/, "aliados en rango"],
    [
      /living creature(?<Plural>s)? touched/,
      "criatura{PLURAL} viva{PLURAL} tocada{PLURAL}",
    ],
    [
      // This is static, there are not such things as 20 projectiles or so
      /50 projectiles \(all of which must be together at the time of casting\)/,
      "50 proyectiles (deben estar todos juntos en el momento del lanzamiento)",
    ],
    [
      /dead creature(?<Plural>s)? touched/,
      "criatura{PLURAL} muerta{PLURAL} tocada",
    ],
    [/(?!un)dead creature(?<Plural>s)?/, "criatura{PLURAL} muerta{PLURAL}"],
    [/creature(?<Plural>s)? touched/, "criatura{PLURAL} tocada{PLURAL}"],
    [/eidolon touched/, "eidolón tocado"],
    [/corpse touched/, "cadáver tocado"],
    [/armor touched/, "armadura tocada"],
    [/shield touched/, "escudo tocado"],
    [/melee weapon touched/, "arma cuerpo a cuerpo tocada"],
    [/weapon touched/, "arma tocada"],
    // Why? Why not an standard?
    [/touched object(?<Plural>s)?/, "objeto{PLURAL} tocado{PLURAL}"],
    [/object(?<Plural>s)? touched/, "objeto{PLURAL} tocado{PLURAL}"],
    [/flask of water touched/, "frasco de agua tocado"],
    [/\sweighing (no more than|up to)\s/, " que no pese más de "],
    [/(?<!no )two(?! of which)/, "dos"],
    [/three/, "tres"],
    [/four/, "cuatro"],
    [/five/, "cinco"],
    [/six/, "seis"],
    [/up to/, "hasta"],
    [/see below/, "ver más abajo"],
    [/nonlawful creatures/, "criaturas no legales"],
    [
      /extraplanar creature(?<Plural>s)?/,
      "criatura{PLURAL} extraplanaria{PLURAL}",
    ],
    [/one ally/, "un aliado"],
    [/ally/, "aliado"],
    [/allies/, "aliados"],
    [/corporeal creature(?<Plural>s)?/, "criatura{PLURAL} corpórea{PLURAL}"],
    [/living creature(?<Plural>s)?/, "criatura{PLURAL} viva{PLURAL}"],
    [/creature(?<Plural>s)?/, "criatura{PLURAL}"],
    [/animal(?<Plural>s)?/, "animal{PLURAL}"],
    [/armor/, "armadura"],
    [/object/, "objeto"],
    [/elemental(?<Plural>s)?/, "elemental{PLURAL}"],
    [/outsider(?<Plural>s)?/, "ajeno{PLURAL}"],
    [/[uU]ndead(?<Plural>s)?/, "muerto{PLURAL} viviente{PLURAL}"],
    [/centered on you(?!r)/, "centrada en ti"],
    [/[Yy]ou(?!r)/, "tú"],
    [/[Yy]our/, "tu"],
    // FIXME: This is assuming the gender on `one`
    [/\sone per\s/, " una por "],
    [/\sper level/, " por nivel"],
    [/\sper\s/, " cada "],
    [/round(?<Plural>s)?/, "asalto{PLURAL}"],
    [/caster level(?<Plural>s)?/, "nivel{PLURAL} de lanzador"],
    [/one\/level/, "una/nivel"],
    [/\/level(?<Plural>s)?/, "/nivel{PLURAL}"],
    [/\slevel(?<Plural>s)?/, " nivel{PLURAL}"],
    [/see text/, "ver texto"],
    [/until discharged/, "hasta ser descargado"],
    [/\sof (?<HD>\d+) HD/, " de {HD} DG"],
    [/\swith (?<HD>\d+) HD/, " con {HD} DG"],
    [/\sor fewer HD/, " o menos DG"],
    [/any number of\s/, "cualquier número de "],
  ]);
  commonTargetsMap.forEach((translationTemplate, originalRegex) => {
    let fullTextRegex = new RegExp(`${originalRegex.source}`, "g");
    if (!originalRegex.source.includes("ToTranslate")) {
      fullTextRegex = new RegExp(
        `(?<ToTranslate>${originalRegex.source})`,
        "g"
      );
    }

    if (fullTextRegex.test(target)) {
      target = translateAllCoincidences(
        fullTextRegex,
        translationTemplate,
        target
      );
    }
  });

  target = _handleDistanceTranslations(target);
  target = _handleWeight(target);
  target = _handleVolume(target);
  target = handleCoordinatingConjunction(target);
  target = _handleCommonMisc(target);
  target = _fixCommonGenderTypos(target);

  return target;
};

/**
 * Translate common area inputs.
 *
 * @param {string} area The area to be translated.
 * @returns {string} Translated area.
 */
export const tArea = (area) => {
  const areaRegexMap = new Map([
    [/cone-shaped emanation/, "emanación en forma de cono"],
    [/cone-shaped burst/, "explosión en forma de cono"],
    [
      /The caster and all allies within a /,
      "El lanzador y todos sus aliados en una ",
    ],
    [/\scentered on the caster/, " centrada en el lanzador"],
    [/circle/, "círculo"],
    [/cylinder/, "cilindro"],
    [/\scentered on you/, " centrada en ti"],
    [/from touched creature/, "desde la criatura tocada"],
    [/all allies/, "todos los aliados"],
    [/foe(?<Plural>s)?/, "enemigo{PLURAL}"],
    [/one solid object/, "un objeto sólido"],
    [/object(?<Plural>s)? touched/, "objeto{PLURAL} tocado{PLURAL}"],
    [/object(?<Plural>s)?/, "objeto{PLURAL}"],
    [/one crystalline creature/, "una criatura cristalina"],
    // FIXME: We need to tackle the issue with gendering
    [/one or more/, "una o más"],
    [/living creature(?<Plural>s)?/, "criatura{PLURAL} viva{PLURAL}"],
    [/creature(?<Plural>s)?/, "criatura{PLURAL}"],
    [/see text/, "ver texto"],
    [/\swithin a\s/, " en una "],
    // Avoiding 30-ft. line, 40 ft. line, etc
    [/(?<!\d+[-\s]ft\.\s)line(?<Plural>s)?/, "línea{PLURAL}"],
    [/\stwo\s/, " dos "],
  ]);
  areaRegexMap.forEach((translationTemplate, originalRegex) => {
    let fullTextRegex = new RegExp(`${originalRegex.source}`, "g");
    if (!originalRegex.source.includes("ToTranslate")) {
      fullTextRegex = new RegExp(
        `(?<ToTranslate>${originalRegex.source})`,
        "g"
      );
    }

    if (fullTextRegex.test(area)) {
      area = translateAllCoincidences(fullTextRegex, translationTemplate, area);
    }
  });

  area = _handleDistanceTranslations(area);
  area = handleCoordinatingConjunction(area);
  area = _fixCommonGenderTypos(area);
  area = _handleCommonMisc(area);

  return area;
};

/**
 * Translates the duration of an spell/class ability/etc... e.g. `24 hours -> 24 horas`
 *
 * @param {string} duration The duration to be translated.
 * @returns {string} The text translated.
 */
export const tDuration = (duration) => {
  const durationMap = new Map([
    [
      /(?<Duration>\d+)(?<ToTranslate>\sround(?<Plural>s)?)/,
      "{DURATION} asalto{PLURAL}",
    ],
    [
      /(?<Duration>\d+)(?<ToTranslate>\s(minute|min\.)(?<Plural>s)?)/,
      "{DURATION} minuto{PLURAL}",
    ],
    [
      /(?<Duration>\d+)(?<ToTranslate>\shour(?<Plural>s)?)/,
      "{DURATION} hora{PLURAL}",
    ],
    [
      /(?<Duration>\d+)(?<ToTranslate>\sday(?<Plural>s)?)/,
      "{DURATION} día{PLURAL}",
    ],
    [/one day/, "un día"],
    [/permanent/, "permanente"],
    [/see below/, "ver más abajo"],
    [/instantaneous/, "instantáneo"],
    [/concentration/, "concentración"],
    [/special/, "especial"],
    [/up to/, "hasta"],
    [/until completed/, "hasta ser completada"],
    [/until you return to your body/, "hasta volver a tu cuerpo"],
    [/until discharged/, "hasta ser descargado"],
    [/until expended/, "hasta agotarse"],
    [/until triggered/, "hasta dispararse"],
    [/\sless/, " menos"],
    [/see below/, "ver más abajo"],
    [/whichever comes first/, "lo que suceda antes"],
  ]);
  durationMap.forEach((translationTemplate, originalRegex) => {
    let fullTextRegex = new RegExp(`${originalRegex.source}`, "g");
    if (!originalRegex.source.includes("ToTranslate")) {
      fullTextRegex = new RegExp(
        `(?<ToTranslate>${originalRegex.source})`,
        "g"
      );
    }

    if (fullTextRegex.test(duration)) {
      duration = translateAllCoincidences(
        fullTextRegex,
        translationTemplate,
        duration
      );
    }
  });

  duration = handleCoordinatingConjunction(duration);
  duration = _handleCommonMisc(duration);

  return duration;
};

/**
 * Translates a given effect from common effects.
 *
 * @param {string} effect The effect to be translated.
 * @returns {string} The translated effect.
 */
export const tEffect = (effect) => {
  const effectMap = new Map([
    [/one summoned creature/, "una criatura convocada"],
    [/magical sensor/, "sensor mágico"],
    [/ray(?<Plural>s)?/, "rayo{PLURAL}"],
    [/one symbol/, "un símbolo"],
    [/one or more/, "uno o más"],
    [/see text/, "ver texto"],
    [
      /fog spreads in 20-ft\. radius/,
      "bruma que se expande en un radio de 20 pies (6 m)",
    ],
    [
      /cloud spreads in 20-ft\. radius/,
      "nube que se expande en un radio de 20 pies (6 m)",
    ],
    [/cylinder/, "cilindro"],
    [/wall/, "muro"],
  ]);
  effectMap.forEach((translationTemplate, originalRegex) => {
    let fullTextRegex = new RegExp(`${originalRegex.source}`, "g");
    if (!originalRegex.source.includes("ToTranslate")) {
      fullTextRegex = new RegExp(
        `(?<ToTranslate>${originalRegex.source})`,
        "g"
      );
    }

    if (fullTextRegex.test(effect)) {
      effect = translateAllCoincidences(
        fullTextRegex,
        translationTemplate,
        effect
      );
    }
  });

  effect = _handleDistanceTranslations(effect);
  effect = handleCoordinatingConjunction(effect);
  effect = _handleCommonMisc(effect);
  return effect;
};

/**
 * Gets common things such as `/level` or `up to` and translates them.
 *
 * @param {string} text The text to translated.
 * @returns {string} Translated text
 */
const _handleCommonMisc = (text) => {
  const miscMap = new Map([
    [/plant (creature|criatura)(?<Plural>s)?/, "criaturas tipo planta"],
    [/see text/, "ver texto"],
    [/level(?<Plural>s)?/, "nivel{PLURAL}"],
    [/(?<!\s)up to\s/, "hasta "],
    [/\sup to\s/, " de hasta "],
    [
      /nonevil (creature|criatura)(?<Plural>s)?/,
      "criatura{PLURAL} no maligna{PLURAL}",
    ],
    [/\d+ (?<ToTranslate>HD)/, "DG"],
    [/\(S\)/, "(Mo)"],
  ]);
  miscMap.forEach((translationTemplate, originalRegex) => {
    let fullTextRegex = new RegExp(`${originalRegex.source}`, "g");
    if (!originalRegex.source.includes("ToTranslate")) {
      fullTextRegex = new RegExp(
        `(?<ToTranslate>${originalRegex.source})`,
        "g"
      );
    }

    if (fullTextRegex.test(text)) {
      text = translateAllCoincidences(fullTextRegex, translationTemplate, text);
    }
  });

  text = _handlePlural(text);
  return text;
};

/**
 * Translate **all** coincidences of given.
 *
 * @param {RegExp} translationRegex The regex to translate
 * @param {string} translationTemplate The template to use for the translation
 * @param {string} text Text to translate.
 * @returns {string} Translated text.
 */
const translateAllCoincidences = (
  translationRegex,
  translationTemplate,
  text
) => {
  const allCoincidences = Array.from(
    text.matchAll(new RegExp(translationRegex.source, "g"))
  );
  allCoincidences.forEach((regexArray, _idx, _array) => {
    let translation = translationTemplate;
    const regexGroups = regexArray.groups;
    // eslint-disable-next-line array-element-newline
    const [duration, HD, toTranslate] = [
      regexGroups?.Duration || "",
      regexGroups?.HD || "",
      regexGroups?.ToTranslate || "",
    ];
    if (!toTranslate) {
      logger(`Regex ${translationRegex} has no "ToTranslate" group!`);
      return;
    }
    let isPlural = Boolean(regexGroups?.Plural);

    // Special treatment for abbreviations
    if (text.includes("min.") && Number(duration) > 1) {
      isPlural = true;
    }

    if (isPlural) {
      translation = _handlePlural(translation);
    } else {
      // We get rid of the {PLURAL} placeholder
      translation = translation.replace(/\{PLURAL\}/g, "");
    }

    if (duration) {
      translation = translation.replace("{DURATION}", duration);
    }

    if (HD) {
      translation = translation.replace("{HD}", HD);
    }

    text = text.replace(`${duration}${toTranslate}`, translation);
  });
  return text;
};

/**
 * Handles translations with distances.
 *
 * @param {string} text The text to be translated with distances.
 * @returns The translated text.
 */
const _handleDistanceTranslations = (text) => {
  const distanceRegexMap = new Map([
    [/(?<Distance>\d+)-ft\. line/, "línea de {DISTANCE} pies ({CONVERSION} m)"],
    [
      // This may come translated from above
      /up to (?<Distance>\d+)-ft\.-radius\/(level|nivel) emanation/,
      "emanación de hasta {DISTANCE} pies ({CONVERSION} m) de radio/nivel",
    ],
    [
      /(?<Distance>\d+)-ft\.-radius emanation/,
      "emanación de {DISTANCE} pies ({CONVERSION} m) de radio",
    ],
    [
      /(?<Distance>\d+)-ft\.-radius spread/,
      "expansión de {DISTANCE} pies ({CONVERSION} m) de radio",
    ],
    [
      /(?<Distance>\d+)-mile-radius (circle|círculo)/,
      "círculo de {DISTANCE} millas ({CONVERSION_MILE} km) de radio",
    ],
    [
      /(?<Distance>\d+) mile(?<Plural>s)?/,
      "{DISTANCE} milla{PLURAL} ({CONVERSION_MILE} km)",
    ],
    [
      /(?<Distance>\d+)-ft\. cone-shaped spread/,
      "expansión de {DISTANCE} pies ({CONVERSION} m) en forma de cono",
    ],
    [
      /(?<Distance>\d+)[-\s](ft\.|foot) [cC]one/,
      "cono de {DISTANCE} pies ({CONVERSION} m)",
    ],
    [
      /(?<Distance>\d+)-ft\.-radius spherical emanation/,
      "emanación esférica de {DISTANCE} pies ({CONVERSION} m) de radio",
    ],
    [
      /(?<Distance>\d+)-ft\.(-|\s)radius burst/,
      "explosión de {DISTANCE} pies ({CONVERSION} m) de radio",
    ],
    [
      /(?<Distance>\d+)(-ft\.|\sfoot|\sfeet) [rR]adius/,
      "{DISTANCE} pies ({CONVERSION} m) de radio",
    ],
    [
      /(?<Distance>\d+)-ft\. burst/,
      "explosión de {DISTANCE} pies ({CONVERSION} m)",
    ],
    [
      // FIXME: This is very circunstancial, it should be done another way
      /one (?<Distance>\d+)-ft\. square/,
      "un cuadrado de {DISTANCE} pies ({CONVERSION} m)",
    ],
    [
      /(?<Distance>\d+)-ft\. square/,
      "cuadrado de {DISTANCE} pies ({CONVERSION} m)",
    ],
    [
      /(?<Distance>\d+) sq\. ft\./,
      "{DISTANCE} pies cuadrados ({CONVERSION_SQ} m2)",
    ],
    [
      /\swith a radius of (?<Distance>\d+) ft\./,
      " con un radio de {DISTANCE} pies ({CONVERSION} m)",
    ],
    [
      // Some spells have 2 spaces in-between for some reason
      /no two of which \s?(can|may) be more\s+than (?<Distance>\d+) ft\. apart/,
      "2 receptores cualesquiera no pueden distar más de {DISTANCE} pies ({CONVERSION} m)",
    ],
    [
      /all within (?<Distance>\d+) ft\. of each other/,
      "2 receptores cualesquiera no pueden distar más de {DISTANCE} pies ({CONVERSION} m)",
    ],
    [
      /(?<Distance>\d+)-ft\. cube(?<Plural>s)?/,
      "cubo{PLURAL} de {DISTANCE} pies ({CONVERSION} m)",
    ],
    [
      /(?<Distance>\d+)(-|\s)ft\.\/(level|nivel) long/,
      "{DISTANCE} pies ({CONVERSION} m)/nivel de largo",
    ],
    [
      /(?<Distance>\d+)(-|\s)ft\. high/,
      "{DISTANCE} pies ({CONVERSION} m) de alto",
    ],
    [
      /(?<Distance>\d+)(-|\s)ft\.\/(level|nivel) high/,
      "{DISTANCE} pies ({CONVERSION} m)/nivel de alto",
    ],
    [/(?<Distance>\d+) ft\./, "{DISTANCE} pies ({CONVERSION} m)"],
  ]);
  distanceRegexMap.forEach((translation, originalRegex, _map) => {
    if (originalRegex.test(text)) {
      const regexGroups = text.match(originalRegex);
      const imperialDistance = regexGroups.groups.Distance;
      const isPlural = Boolean(regexGroups.groups.Plural);
      text = text.replace(
        originalRegex,
        translation
          .replace("{DISTANCE}", imperialDistance)
          .replace("{CONVERSION}", feetToMeters(imperialDistance))
          .replace("{CONVERSION_SQ}", sqFeetToSQMeters(imperialDistance))
          .replace("{CONVERSION_MILE}", mileToKm(imperialDistance))
          .replace("{PLURAL}", isPlural ? "s" : "")
      );
    }
  });
  return text;
};

const _handleWeight = (text) => {
  const weightMap = new Map([
    [/(?<Weight>\d+) lbs\./, "{WEIGHT} libras ({CONVERSION} kg)"],
  ]);
  weightMap.forEach((translation, originalRegex, _map) => {
    if (originalRegex.test(text)) {
      const regexGroups = text.match(originalRegex);
      const weight = regexGroups.groups.Weight;
      const isPlural = Boolean(regexGroups.groups.Plural);
      text = text.replace(
        originalRegex,
        translation
          .replace("{WEIGHT}", weight)
          .replace("{CONVERSION}", poundToKg(weight))
          .replace("{PLURAL}", isPlural ? "s" : "")
      );
    }
  });
  return text;
};

/**
 * Converts cubic feet to cubic meters.
 *
 * @param {string} text The text with volume to translate.
 * @returns {string} Translated text.
 */
const _handleVolume = (text) => {
  const volumeMap = new Map([
    [/(?<Volume>\d+) cu\. ft\./, "{VOLUME} pies cúbicos ({CONVERSION} m3)"],
  ]);
  volumeMap.forEach((translation, originalRegex, _map) => {
    if (originalRegex.test(text)) {
      const regexGroups = text.match(originalRegex);
      const volume = regexGroups.groups.Volume;
      const isPlural = Boolean(regexGroups.groups.Plural);
      text = text.replace(
        originalRegex,
        translation
          .replace("{VOLUME}", volume)
          .replace("{CONVERSION}", cuFeetToCuMeters(volume))
          .replace("{PLURAL}", isPlural ? "s" : "")
      );
    }
  });
  return text;
};

/**
 * Checks the coordinating conjunction and changes it if needed.
 *
 * @param {string} text The coordinating conjunction. e.g. 'o', 'y', ...
 * @returns {string} Translated text (only conjunction have been translated).
 */
export const handleCoordinatingConjunction = (text) => {
  const coordinatingConjunctionMap = new Map([
    [/\sand\s/, " y "],
    // Normal `or less`
    [/\sor less/, " o menos"],
    [/\sor\s/, " o "],
    [/\sthen\s/, " luego "],
    [/\splus\s/, " más "],
    // FIXME: This is feminine by default but is not correct
    [/\sin a\s/, " en una "],
    [/\swith\s/, " con "],
    // FIXME: This is feminine by default but is not correct
    [/\swithin a\s/, " en una "],
    [/\swithin\s/, " en "],
    [/\sof\s/, " de "],
  ]);
  coordinatingConjunctionMap.forEach((translation, originalRegex) => {
    const nextWordRegex = /(?<NextWord>\w+)?/;
    const fullTextRegex = new RegExp(
      `(${originalRegex.source})` + nextWordRegex.source
    );
    if (originalRegex.test(text)) {
      Array.from(
        text.matchAll(
          // FIXME: Since the only way to avoid empty arrays for Regex on `matchAll`
          // is with this hot-constructor we should rearrange all into a refactor
          new RegExp(fullTextRegex.source, "g")
        ),
        (regexArray) => {
          const regexGroups = regexArray.groups;
          // eslint-disable-next-line array-element-newline
          const nextWord = regexGroups.NextWord || "";

          // In Spanish when using `o` we need to make sure next letter is not an `o`
          // otherwise is `u`
          if (translation === " o " && nextWord.startsWith("o")) {
            translation = " u ";
          }
          // In Spanish when using `y` we need to make sure next letter is not an `i` or `hi`
          // otherwise is `e`
          if (translation === " y " && nextWord.startsWith("i")) {
            translation = " e ";
          }

          text = text.replace(originalRegex, translation);
        }
      );
    }
  });
  return text;
};

/**
 * Pluralize words. In order for this to work the regex needs to have a `(?<Plural>s)?`
 * termination for the word to be pluralized and on the translation the keyword `{PLURAL}`
 * needs to be at the end of the translation to be pluralized.
 *
 * @example
 * const arrayWithTrans = [/Creature(?<Plural>s)?/, "Criatura{PLURAL}"]
 *
 * @param {string} text The word to be pluralized.
 * @returns {string} The pluralized word.
 */
const _handlePlural = (text) => {
  Array.from(text.matchAll(/\s?\w+\{PLURAL\}\s?/g), (wordMatch) => {
    if (!wordMatch) {
      logger(`No word matches the pluralization regex. -> ${text}`);
      return;
    }

    let word = wordMatch[0];
    let pluralizedWord;
    // This map contains word -> pluralization e.g. `nivel -> niveles`
    const specialPluralsMap = new Map([
      ["nivel", "es"],
      ["/nivel", "es"],
      ["elemental", "es"],
      ["corporal", "es"],
    ]);
    const simplifiedKey = word.trim().replace("{PLURAL}", "");
    if (specialPluralsMap.has(simplifiedKey)) {
      pluralizedWord = word.replace(
        "{PLURAL}",
        specialPluralsMap.get(simplifiedKey)
      );
    } else {
      pluralizedWord = word.replace("{PLURAL}", "s");
    }
    text = text.replace(word, pluralizedWord);
  });
  return text;
};

/**
 * Resolve some gender typos.
 *
 * @param {string} text The text to be fixed.
 * @returns {string} The text with fixed typos.
 */
const _fixCommonGenderTypos = (text) => {
  // FIXME: Really stupid hotfix for a very specific situation that should be solved
  // by the gender system
  const genderProblemsMap = new Map([
    ["círculo, centrada en ti", "círculo, centrado en ti"],
    ["uno criatura", "una criatura"],
  ]);
  genderProblemsMap.forEach((fix, typo) => {
    text = text.replace(typo, fix);
  });
  return text;
};
