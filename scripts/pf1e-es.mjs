// Credits to https://github.com/DjLeChuck
// https://github.com/DjLeChuck/foundryvtt-pf1-fr-babele/blob/main/scripts/register.js
import * as compendiumBrowserFilters from "./compendiumBrowserFilters.mjs";

import {
  translateBloodlines,
  translateDomains,
  translateRaceSubTypes,
  translateSubDomains,
  translateSubSchool,
} from "./automaticConverters.mjs";
import { actions } from "./actions.mjs";
import { contextNotes } from "./contextNotes.mjs";
import { tScriptCalls } from "./scriptCallsConverter.mjs";
import { tContainerItems } from "./containers.mjs";

Hooks.on("init", () => {
  game.settings.register("pf1e-es", "autoRegisterBabel", {
    name: "Activación automática de la traducción a través de Babele",
    hint: "Establece automáticamente las traducciones dentro de Babele sin tener que hacer referencia al directorio con las traducciones.",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
    /**
     * @param {boolean} value Indica si se ha puesto este valor como `True`
     */
    onChange: (value) => {
      if (value) {
        autoRegisterBabel();
      }

      window.location.reload();
    },
  });

  game.settings.register("pf1e-es", "enableDebug", {
    name: "Activar el modo debug",
    hint: "Mostrará, además de como error por consola, como notificación cualquier error encontrado durante la traducción.",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
  });

  game.babele.registerConverters({
    actions: actions,
    contextNotes: contextNotes,
    scriptCalls: tScriptCalls,
    subSchool: translateSubSchool,
    raceSubTypes: translateRaceSubTypes,
    domains: translateDomains,
    subDomains: translateSubDomains,
    bloodlines: translateBloodlines,
    containerItems: tContainerItems,
  });

  if (game.settings.get("pf1e-es", "autoRegisterBabel")) {
    autoRegisterBabel();
  }
});

Hooks.once("init", async () => {
  if (game.i18n.lang !== "es") return;

  // We load the partials
  const templates = ["text-filter"];
  await loadTemplates(
    templates.map((t) => `modules/pf1e-es/templates/${t}.hbs`)
  );

  // Add new filters
  const browsers = [
    pf1.applications.compendiumBrowser.ClassBrowser,
    pf1.applications.compendiumBrowser.ItemBrowser,
    pf1.applications.compendiumBrowser.SpellBrowser,
    pf1.applications.compendiumBrowser.FeatBrowser,
    pf1.applications.compendiumBrowser.RaceBrowser,
    pf1.applications.compendiumBrowser.CreatureBrowser,
    pf1.applications.compendiumBrowser.BuffBrowser,
  ];
  browsers.forEach((browser) => {
    browser.filterClasses.splice(
      0,
      0,
      compendiumBrowserFilters.OriginalNameFilter
    );
  });
});

Hooks.on("pf1PostReady", () => {
  if (game.i18n.lang !== "es") return;
  /** @type {Record.<string, pf1.registry.Source>} */
  const partialTranslations = {
    PZO1110: { name: "Reglas básicas", pages: 575 },
    PZO1112: { name: "Bestiario 1", pages: 329 },
    PZO1115: { name: "Guía del jugador avanzada", pages: 337, abbr: "GJA" },
    PZO1116: { name: "Bestiario 2", pages: 321 },
    PZO1117: { name: "Magia definitiva", pages: 257, abbr: "MD" },
    PZO1118: { name: "Combate definitivo", pages: 255, abbr: "CD" },
    PZO9226: { name: "Guía del mundo Mar Interior", pages: 320, abbr: "GMI" },
    // Sendas de aventuras
    PZO1002: {
      name: "El Auge de los Señores de las Runas Edición Aniversario",
      pages: 429,
    },
    PZO1021: {
      name: "La Maldición del Trono Carmesí",
      pages: 480,
    },
    PZO9025: {
      name: "Concejo de ladrones 1: Los bastardos del Érebo",
      pages: 96,
    },
    PZO9026: {
      name: "Concejo de ladrones 2: El juicio de las seis pruebas",
      pages: 96,
    },
    PZO9027: {
      name: "Concejo de ladrones 3: Lo que yace en el polvo",
      pages: 96,
    },
    PZO9028: {
      name: "Concejo de ladrones 4: El síndrome infernal",
      pages: 96,
    },
    PZO9029: {
      name: "Concejo de ladrones 5: La Madre de las Moscas",
      pages: 96,
    },
    PZO9030: {
      name: "Concejo de ladrones 6: El Príncipe Doblemente Condenado",
      pages: 96,
    },
    PZO9031: {
      name: "Forjador de reyes 1: Tierra robada",
      pages: 92,
    },
    PZO9032: {
      name: "Forjador de reyes 2: Los ríos se tiñen de rojo",
      pages: 92,
    },
    PZO9033: {
      name: "Forjador de reyes 3: La desaparición de Fuerte de Varn",
      pages: 92,
    },
    PZO9034: {
      name: "Forjador de reyes 4: Sangre por sangre",
      pages: 92,
    },
    PZO9035: {
      name: "Forjador de reyes 5: La guerra de los Reyes Fluviales",
      pages: 92,
    },
    PZO9036: {
      name: "Forjador de reyes 6: El sonido de un millar de gritos",
      pages: 92,
    },
    PZO9037: {
      name: "La calavera de la Serpiente 1: Almas Picho Contrabandista",
      pages: 96,
    },
    PZO9038: {
      name: "La calavera de la Serpiente 2: Carrera hacia el desastre",
      pages: 96,
    },
    PZO9039: {
      name: "La calavera de la Serpiente 3: La ciudad de las 7 lanzas",
      pages: 96,
    },
    PZO9040: {
      name: "La calavera de la Serpiente 4: Las Bóvedas de la Locura",
      pages: 96,
    },
    PZO9041: {
      name: "La calavera de la Serpiente 5: Los Mil Colmillos por Abajo",
      pages: 96,
    },
    PZO9042: {
      name: "La calavera de la Serpiente 6: El santuario del Dios Serpiente",
      pages: 96,
    },
    PZO9043: {
      name: "La Corona de Carroña 1: Las apariciones de Piedra Atormentada",
      pages: 96,
    },
    PZO9044: {
      name: "La Corona de Carroña 2: El juicio de la Bestia",
      pages: 96,
    },
    PZO9045: {
      name: "La Corona de Carroña 3: Luna rota",
      pages: 96,
    },
    PZO9046: {
      name: "La Corona de Carroña 4: La estela del vigilante",
      pages: 96,
    },
    PZO9047: {
      name: "La Corona de Carroña 5: Cenizas al amanecer",
      pages: 96,
    },
    PZO9048: {
      name: "La Corona de Carroña 6: Sombras de la Espira del Patíbulo",
      pages: 96,
    },
    PZO9049: {
      name: "La regente de Jade 1: El legado de Muro de Salmuera",
      pages: 96,
    },
    PZO9050: {
      name: "La regente de Jade 2: La noche de las Sombras Heladas",
      pages: 96,
    },
    PZO9051: {
      name: "La regente de Jade 3: La tormenta voraz",
      pages: 96,
    },
    PZO9052: {
      name: "La regente de Jade 4: El bosque de los espíritus",
      pages: 96,
    },
    PZO9053: {
      name: "La regente de Jade 5: Marea de honor",
      pages: 96,
    },
    PZO9054: {
      name: "La regente de Jade 6: El trono vacío",
      pages: 96,
    },
    PZO9055: {
      name: "Calaveras y Grilletes 1: El motín del Amargura",
      pages: 96,
    },
    PZO9056: {
      name: "Calaveras y Grilletes 2: Saqueadores del mar Febril",
      pages: 96,
    },
    PZO9057: {
      name: "Calaveras y Grilletes 3: Se desata la tempestad",
      pages: 96,
    },
    PZO9058: {
      name: "Calaveras y Grilletes 4: La isla de los Ojos Vacíos",
      pages: 96,
    },
    PZO9059: {
      name: "Calaveras y Grilletes 5: El precio de la infamia",
      pages: 96,
    },
    PZO9060: {
      name: "Calaveras y Grilletes 6: Del corazón del infierno",
      pages: 96,
    },
    PZO9061: {
      name: "La Estrella Fragmentada 1: Fragmentos de pecado",
      pages: 96,
    },
    PZO9062: {
      name: "La Estrella Fragmentada 2: La maldición de la Luz de la Dama",
      pages: 96,
    },
    PZO9063: {
      name: "La Estrella Fragmentada 3: La Piedra el Manicomio",
      pages: 96,
    },
    PZO9064: {
      name: "La Estrella Fragmentada 4: Más allá de la Puerta del Día del Juicio",
      pages: 96,
    },
    PZO9065: {
      name: "La Estrella Fragmentada 5: La Grieta de las Pesadilla",
      pages: 96,
    },
    PZO9066: {
      name: "La Estrella Fragmentada 6: El Corazón Muerto de Xin",
      pages: 96,
    },
    PZO90133: {
      name: "El retorno de los Señores de las Runas 1: Secretos de la Cala de Roderic",
      pages: 94,
    },
    PZO90134: {
      name: "El retorno de los Señores de las Runas 2: Lo que salió de Montaña Hueca",
      pages: 94,
    },
    PZO90135: {
      name: "El retorno de los Señores de las Runas 3: Plaga Rúnica",
      pages: 94,
    },
    PZO90136: {
      name: "El retorno de los Señores de las Runas 4: El Templo del Espíritu del Pavo Real",
      pages: 94,
    },
    PZO90137: {
      name: "El retorno de los Señores de las Runas 5: La ciudad fuera del tiempo",
      pages: 92,
    },
    PZO90138: {
      name: "El retorno de los Señores de las Runas 6: El auge de la nueva Thassilon",
      pages: 92,
    },
  };
  // eslint-disable-next-line array-element-newline
  for (const [key, value] of Object.entries(partialTranslations)) {
    const entry = pf1.registry.sources.get(key);
    if (entry) entry.updateSource(value);
  }
});

function autoRegisterBabel() {
  if (typeof Babele !== "undefined") {
    game.babele.register({
      module: "pf1e-es",
      lang: "es",
      dir: "translations",
    });
  }
}

// Quench tests
Hooks.on("quenchReady", async (quench) => {
  try {
    const { registerTests } = await import("./quenchSetup.mjs");
    registerTests(quench);
  } catch (err) {
    console.error(
      "Este error es esperado si tienes el módulo Quench activado, ya que al instalar Spanish Compendium todos los tests son excluidos por razones de optimización."
    );
    console.error(err);
  }
});
