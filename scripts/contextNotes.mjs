/**
 * @typedef ContextNote Object that represents a PF1e System Context Note
 * @prop {string} formula
 * @prop {string} modifier
 * @prop {string} operator
 * @prop {number} priority
 * @prop {string} subTarget
 * @prop {string} target
 * @prop {string} text
 * @prop {number} value
 *
 * @typedef TranslatedContextNote Object that represents the translation of a Context Note
 * @prop {string} text The translated text
 */

import { logger } from "./utils.mjs";

/**
 * Converter for context Notes on Changes.
 *
 * @param {Array<ContextNote>} contextNotes List of original Context Notes
 * @param {Array<TranslatedContextNote> | undefined} translations List of Context Notes translated
 * @returns {Array<ContextNote>} Context Notes translated
 */
export const contextNotes = (contextNotes, translations) => {
  // If there isn't any context note there's no point on checking the missing ones
  if (contextNotes.length > 0) {
    // Some context notes don't have text within but we don't care about them
    const ctxNotesWithText = contextNotes.filter((ctxNote) =>
      Object.prototype.hasOwnProperty.call(ctxNote, "text")
    );
    if (translations?.length !== ctxNotesWithText.length) {
      logger(
        `Faltan traducciones para las Notas de contexto -> ${translations?.length || 0}/${ctxNotesWithText.length}`
      );
      return contextNotes;
    }
  }

  contextNotes.forEach((_obj, index, _contextNotes) => {
    const data = translations[index];
    contextNotes[index].text = data.text;
  });

  return contextNotes;
};
