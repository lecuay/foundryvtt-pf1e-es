import { handleCoordinatingConjunction } from "./common.mjs";

/**
 * SubSchools are pretty standard we can automatize those.
 *
 * @param {string} subschool
 * @returns {string} Translated subschool or original
 */
export const translateSubSchool = (subschool) => {
  const subSchoolMap = new Map([
    ["calling", "llamada"],
    ["charm", "hechizo"],
    ["compulsion", "compulsión"],
    ["creation", "creación"],
    ["figment", "quimera"],
    ["glamer", "engaño"],
    ["haunted", "embrujo"],
    ["healing", "curación"],
    ["pattern", "pauta"],
    ["phantasm", "fantasmagoría"],
    ["polymorph", "polimorfia"],
    ["scrying", "escrudiñamiento"],
    ["shadow", "sombra"],
    ["summoning", "convocación"],
    ["teleportation", "teletransporte"],
  ]);
  subSchoolMap.forEach((translation, original) => {
    subschool = subschool.replace(original, translation);
  });

  subschool = handleCoordinatingConjunction(subschool);

  return subschool;
};

/**
 * Translates the static domains.
 *
 * @param {Object.<string, number>} domains Original domains
 * @return {Object.<string, number>} The translated domains
 */
export const translateDomains = (domains) => {
  const domainMap = new Map([
    ["Air", "Aire"],
    ["Artifice", "Artificio"],
    ["Chaos", "Caos"],
    ["Charm", "Encantamiento"],
    ["Community", "Comunidad"],
    ["Darkness", "Oscuridad"],
    ["Death", "Muerte"],
    ["Destruction", "Destrucción"],
    ["Earth", "Tierra"],
    ["Evil", "Mal"],
    ["Fire", "Fuego"],
    ["Glory", "Gloria"],
    ["Good", "Bien"],
    ["Healing", "Curación"],
    ["Knowledge", "Saber"],
    ["Law", "Legal"],
    ["Liberation", "Liberación"],
    ["Luck", "Suerte"],
    ["Madness", "Locura"],
    ["Magic", "Magia"],
    ["Nobility", "Nobleza"],
    ["Plant", "Planta"],
    ["Protection", "Protección"],
    ["Repose", "Reposo"],
    ["Ruins", "Ruinas"],
    ["Rune", "Runa"],
    ["Strength", "Fuerza"],
    ["Sun", "Sol"],
    ["Travel", "Viaje"],
    ["Trickery", "Engaño"],
    ["Void", "Vacío"],
    ["War", "Guerra"],
    ["Water", "Agua"],
    ["Weather", "Clima"],
  ]);
  domainMap.forEach((translation, original) => {
    if (Object.prototype.hasOwnProperty.call(domains, original)) {
      domains[translation] = domains[original];
      delete domains[original];
    }
  });
  return domains;
};

/**
 * Translates the static subdomains.
 *
 * @param {Object.<string, number>} subDomain Original subdomain name.
 * @returns {Object.<string, number>} Translated subdomain.
 */
export const translateSubDomains = (subDomain) => {
  const subDomainMap = new Map([
    ["Aeon", "Eón"],
    ["Alchemy", "Alquimia"],
    ["Ambush", "Emboscada"],
    ["Ancestors", "Antepasados"],
    ["Arcane", "Arcano"],
    ["Aristocracy", "Aristocracia"],
    ["Arson", "Incendiario"],
    ["Ash", "Ceniza"],
    ["Blood", "Sangre"],
    ["Cannibalism", "Canibalismo"],
    ["Catastrophe", "Catástrofe"],
    ["Caves", "Cuevas"],
    ["Cloud", "Nubes"],
    ["Competition", "Competencia"],
    ["Construct", "Constructo"],
    ["Cooperation", "Cooperación"],
    ["Corruption", "Corrupción"],
    ["Curse", "Maldición"],
    ["Daemon", "Demoníacos"],
    ["Day", "Día"],
    ["Decay", "Decadencia"],
    ["Deception", "Decepción"],
    ["Defense", "Defensa"],
    ["Demon", "Demonios"],
    ["Devil", "Diablos"],
    ["Divine", "Divino"],
    ["Dragon", "Dragones"],
    ["Duels", "Duelos"],
    ["Education", "Educación"],
    ["Entropy", "Entropía"],
    ["Espionage", "Espionaje"],
    ["Exploration", "Exploración"],
    ["Family", "Familia"],
    ["Fate", "Destino"],
    ["Fear", "Miedo"],
    ["Feather", "Plumas"],
    ["Ferocity", "Ferocidad"],
    ["Fist", "Puños"],
    ["Flotsam", "Restos flotantes"],
    ["Flowing", "Corriente"],
    ["Fortifications", "Fortificaciones"],
    ["Freedom", "Libertad"],
    ["Friendship", "Amistad"],
    ["Fur", "Pelaje"],
    ["Greed", "Avaricia"],
    ["Growth", "Crecimiento"],
    ["Hatred", "Odio"],
    ["Heroism", "Heroísmo"],
    ["Home", "Hogar"],
    ["Hubris", "Arrogancia"],
    ["Ice", "Hielo"],
    ["Imagination", "Imaginación"],
    ["Industry", "Industria"],
    ["Innuendo", "Insinuación"],
    ["Insanity", "Demencia"],
    ["Insect", "Insectos"],
    ["Isolation", "Aislamiento"],
    ["Judgment", "Juicio"],
    ["Language", "Idioma"],
    ["Leadership", "Liderazgo"],
    ["Legislation", "Ley"],
    ["Light", "Luz"],
    ["Loss", "Pérdida"],
    ["Love", "Amor"],
    ["Loyalty", "Lealtad"],
    ["Lust", "Lujuria"],
    ["Martyr", "Mártires"],
    ["Medicine", "Medicina"],
    ["Memory", "Memoria"],
    ["Moon", "Luna"],
    ["Murder", "Asesinato"],
    ["Night", "Noche"],
    ["Nightmare", "Pesadilla"],
    ["Oceans", "Océanos"],
    ["Petrification", "Petrificación"],
    ["Plague", "Plaga"],
    ["Protean", "Proteano"],
    ["Purity", "Pureza"],
    ["Radiation", "Radiación"],
    ["Rage", "Furia"],
    ["Redemption", "Redención"],
    ["Resolve", "Resolución"],
    ["Restoration", "Restauración"],
    ["Resurrection", "Resurrección"],
    ["Revelation", "Revelación"],
    ["Revelry", "Jolgorio"],
    ["Revolution", "Revolución"],
    ["Riot", "Disturbio"],
    ["Rites", "Rituales"],
    ["Rivers", "Ríos"],
    ["Seasons", "Estaciones"],
    ["Self-Realization", "Autodescubrimiento"],
    ["Slavery", "Esclavitud"],
    ["Smoke", "Humo"],
    ["Solitude", "Solicitud"],
    ["Souls", "Almas"],
    ["Stars", "Estrellas"],
    ["Storms", "Tormentas"],
    ["Tactics", "Tácticas"],
    ["Thievery", "Robo"],
    ["Thought", "Pensamiento"],
    ["Toil", "Trabajo duro"],
    ["Torture", "Tortura"],
    ["Trade", "Comercio"],
    ["Trap", "Trampas"],
    ["Tyranny", "Tiranía"],
    ["Undead", "Muerto viviente"],
    ["Venom", "Veneno"],
    ["Wards", "Guardias"],
    ["Whimsy", "Capricho"],
    ["Wind", "Viento"],
  ]);
  subDomainMap.forEach((translation, original) => {
    if (Object.prototype.hasOwnProperty.call(subDomain, original)) {
      subDomain[translation] = subDomain[original];
      delete subDomain[original];
    }
  });
  return subDomain;
};

/**
 * Translates the Bloodlines object.
 *
 * @param {Object.<string, number>} bloodlines Original names.
 * @returns {Object.<string, number>} Translated bloodlines.
 */
export const translateBloodlines = (bloodlines) => {
  const bloodlinesMap = new Map([
    ["Abyssal", "Abisal"],
    ["Accursed", "Maldito"],
    ["Aquatic", "Acuático"],
    ["Arcane", "Arcano"],
    ["Daemon", "Demónico"],
    ["Destined", "Destinado"],
    ["Draconic", "Dracónico"],
    ["Dreamspun", "Tejido de los sueños"],
    ["Ectoplasm", "Ectoplasma"],
    ["Efreeti", "Ifriti"],
    ["Fey", "Feérico"],
    ["Ghoul", "Macabro"],
    ["Harrow", "Grada"],
    ["Imperious", "Imperioso"],
    ["Impossible", "Imposible"],
    ["Martyred", "Martirizado"],
    ["Orc", "Orco"],
    ["Pestilence", "Pestilencia"],
    ["Protean", "Proteano"],
    ["Psychic", "Psíquico"],
    ["Serpentine", "Serpentina"],
    ["Shadow", "Sombra"],
    ["Starsoul", "Alma estelar"],
    ["Stormborn", "Nacido de la tormenta"],
    ["Undead", "Muerto viviente"],
    ["Verdant", "Verde"],
  ]);
  bloodlinesMap.forEach((translation, original) => {
    if (Object.prototype.hasOwnProperty.call(bloodlines, original)) {
      bloodlines[translation] = bloodlines[original];
      delete bloodlines[original];
    }
  });
  return bloodlines;
};

/**
 * Subtypes for races can be translated.
 *
 * @param {Array<string>} raceSubTypes Original race subtype.
 * @returns {Array<string>} Translated race subtype.
 */
export const translateRaceSubTypes = (raceSubTypes) => {
  const raceSubTypeMap = new Map([
    ["Android", "Androide"],
    ["Aquatic", "Acuático"],
    ["Catfolk", "Félidos"],
    ["Dark folk", "Sombrío"],
    ["Dwarf", "Enano"],
    ["Elf", "Elfo"],
    ["Gnome", "Gnomo"],
    ["Halfing", "Mediano"],
    ["Human", "Humano"],
    ["Native", "Nativo"],
    ["Orc", "Orco"],
  ]);
  for (let index = 0; index < raceSubTypes.length; index++) {
    const element = raceSubTypes[index];
    raceSubTypes[index] = raceSubTypeMap.get(element) || element;
  }
  return raceSubTypes;
};
