import {
  tArea,
  tDuration,
  tEffect,
  tSavingThrowType,
  tTarget,
  tActionName,
} from "./common.mjs";
import { logger } from "./utils.mjs";

/**
 * Wrapper for common calls when there are missing translations.
 *
 * @param {Array.<ItemActionRaw>} actions - Array of actions associated to the item.
 * @param {Array.<TranslatedAction>} translations - Array of translations for the actions.
 */
const missingActionsLog = (actions, translations) => {
  logger(
    `¡Traducción parcial!\nEl número de acciones y las traducciones de estas no coinciden (${translations.length}/${actions.length}).\nPor favor, comunique este error al desarrollador.\n\nAcciones: ${actions.map((action) => `"${action.name}"` || "Nombre desconocido").join(", ")}`
  );
};

/**
 * A list of wrappers to check that actions are valid for translations.
 *
 * @param {Array.<ItemActionRaw>} actions The actions linked to that Class Ability.
 * @param {Array.<TranslatedAction>} translations The translated action.
 * @returns {boolean} Whether the actions are valid for translation.
 */
const actionsChecks = (actions, translations) => {
  // If there are no actions let's just continue
  if (actions.length === 0) {
    return false;
  }
  // If translations is undefined let's just continue
  if (!translations) {
    return false;
  }
  // Check if the actions and translations have the same length
  if (
    actions.length > 1 &&
    actions.length !== translations.length &&
    // let's check if the action is worth to be translated
    actions.every(
      (ac) =>
        // First of all let's make sure action name is not "Use" or "Attack"
        // Since many weapons and abilities have "Use" or "Attack" but nothing else
        !["Use", "Attack"].includes(ac.name) &&
        // Then we search for all the properties that we need to translate
        ac.attackName &&
        ac.description &&
        ac.duration?.units === "spec" &&
        ac.target?.value &&
        ac.save?.description &&
        ac.spellEffect &&
        ac.area &&
        ac.range?.units === "spec" &&
        ac.effectNotes &&
        ac.conditionals &&
        ac.attackNotes &&
        ac.extraAttacks?.formula?.label
    )
  ) {
    missingActionsLog(actions, translations);
    // We don't return false because there empty translations such as [{}] that works as templates for
    // the automation to happen
  }

  return true;
};

/**
 * Translates class abilities actions.
 *
 * @param {Array.<ItemActionRaw>} actions The actions linked to that Class Ability.
 * @param {Array.<TranslatedAction>} [translations=[]] The translated action.
 *
 * @returns {Array.<ItemActionRaw>} actions The same received actions but translated.
 */
export const actions = (actions, translations = []) => {
  // Checks
  if (!actionsChecks(actions, translations)) {
    return actions;
  }

  actions.forEach((currentAction, index, _actionsArray) => {
    const tAction = translations[index];

    if (currentAction.name) {
      if (!tAction?.name) {
        currentAction.name = tActionName(currentAction.name);
      } else {
        currentAction.name = tAction.name;
      }
    }

    if (currentAction.attackName) {
      if (!tAction?.attackName) {
        currentAction.attackName = tActionName(currentAction.attackName);
      } else {
        currentAction.attackName = tAction.attackName;
      }
    }

    if (currentAction.description && tAction?.description) {
      currentAction.description = tAction.description;
    }

    if (
      currentAction.duration?.units === "spec" &&
      currentAction.duration?.value
    ) {
      if (!tAction?.duration) {
        currentAction.duration.value = tDuration(currentAction.duration.value);
      } else {
        currentAction.duration.value = tAction.duration;
      }
    }

    if (currentAction.target?.value) {
      if (!tAction?.target) {
        currentAction.target.value = tTarget(currentAction.target.value);
      } else {
        currentAction.target.value = tAction.target;
      }
    }

    if (currentAction.save?.description) {
      if (!tAction?.save) {
        currentAction.save.description = tSavingThrowType(
          currentAction.save.description
        );
      } else {
        currentAction.save.description = tAction.save;
      }
    }

    if (currentAction.spellEffect) {
      if (!tAction?.spellEffect) {
        currentAction.spellEffect = tEffect(currentAction.spellEffect);
      } else {
        currentAction.spellEffect = tAction.spellEffect;
      }
    }

    if (currentAction.area) {
      if (!tAction?.spellArea) {
        currentAction.area = tArea(currentAction.area);
      } else {
        currentAction.area = tAction.spellArea;
      }
    }

    if (currentAction.range?.units === "spec" && currentAction.range?.value) {
      if (!tAction?.range) {
        currentAction.range.value = tArea(currentAction.range.value);
      } else {
        currentAction.range.value = tAction.range;
      }
    }

    if (
      currentAction.effectNotes &&
      currentAction.effectNotes?.length > 0 &&
      currentAction.effectNotes?.some((effectNote) => effectNote)
    ) {
      // If action comes with effect notes but no translation or translation is not the same length
      if (tAction?.effectNotes?.length !== currentAction.effectNotes.length) {
        logger(
          `Faltan traducciones para las Notas de Efecto en la acción "${currentAction.name}" -> ${
            tAction?.effectNotes?.length || 0
          }/${currentAction.effectNotes.length}`
        );
      } else {
        actions[index].effectNotes = tAction.effectNotes;
      }
    }

    if (currentAction.conditionals && currentAction.conditionals?.length > 0) {
      // If action comes with conditionals but no translation or translation is not the same length
      if (tAction?.conditionals?.length !== currentAction.conditionals.length) {
        logger(
          `Faltan traducciones para los Modificadores Condicionales en la acción "${currentAction.name}" -> ${
            tAction?.conditionals?.length || 0
          }/${currentAction.conditionals.length}`
        );
      } else {
        actions[index].conditionals.forEach((conditional, idx) => {
          const tConditional = tAction.conditionals[idx];
          conditional.name = tConditional.name;
        });
      }
    }

    if (currentAction.attackNotes && currentAction.attackNotes?.length > 0) {
      // If action comes with attack notes but no translation or translation is not the same length
      if (tAction?.attackNotes?.length !== currentAction.attackNotes.length) {
        logger(
          `Faltan traducciones para las Notas en la acción "${currentAction.name}" -> ${
            tAction?.attackNotes?.length || 0
          }/${currentAction.attackNotes.length}`
        );
      } else {
        actions[index].attackNotes = tAction.attackNotes;
      }
    }

    if (currentAction.extraAttacks?.formula?.label) {
      actions[index].extraAttacks.formula.label = tActionName(
        currentAction.extraAttacks.formula.label
      );
    }
  });

  return actions;
};
