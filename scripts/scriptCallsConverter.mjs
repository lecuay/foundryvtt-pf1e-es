/**
 * @typedef ScriptCall
 * @prop {string} _id
 * @prop {string} name
 * And more...
 *
 * @typedef ScriptCallTranslation
 * @prop {string} name
 */

import { logger } from "./utils.mjs";

/**
 *
 * @param {Array.<ScriptCall>} scriptCalls List of script calls to be listed.
 * @param {Array.<ScriptCallTranslation>} translations List of script calls translated.
 * @returns {Array.<ScriptCall>} The translated script calls.
 */
export const tScriptCalls = (scriptCalls, translations) => {
  if (!translations) {
    return scriptCalls;
  }

  const scriptCallsWithText = scriptCalls.filter((sc) =>
    Object.prototype.hasOwnProperty.call(sc, "name")
  );
  if (translations.length !== scriptCallsWithText.length) {
    logger(
      `Faltan traducciones para los Script Calls -> ${translations.length}/${scriptCallsWithText.length}`
    );
    return scriptCalls;
  }

  scriptCalls.forEach((scriptCall, index, _array) => {
    const data = translations[index];
    scriptCall.name = data.name;
  });

  return scriptCalls;
};
