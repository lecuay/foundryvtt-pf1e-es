declare interface ScriptCallRaw {
  _id: string;
  name?: string;
}

declare interface TranslatedScriptCall {
  name?: string;
}
