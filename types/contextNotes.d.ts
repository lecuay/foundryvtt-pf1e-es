declare interface ContextNote {
  formula: string;
  modifier: string;
  operator: string;
  priority: number;
  subTarget: string;
  target: string;
  text: string;
  value: number;
}

declare interface TranslatedContextNote {
  text: string;
}
