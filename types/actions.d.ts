/**
 * Action data for a {@link pf1.documents.ItemPF}
 */
declare interface ItemActionRaw {
  _id: string;
  actionType?: string;
  area?: string;
  attackName?: string;
  attackNotes?: string[];
  conditionals?: {
    _id: string,
    name?: string,
  }[];
  description?: string;
  duration?: {
    units?: string,
    value?: string,
  };
  effectNotes?: string[];
  extraAttacks?: {
    formula?: {
      bonus?: string,
      count?: string,
      label?: string,
    },
    type?: string,
  };
  name: string;
  range?: {
    units?: string,
    value?: string,
  };
  save?: {
    description?: string,
    harmless?: boolean,
    type?: string,
  };
  spellEffect?: string;
  target?: {
    value?: string,
  };
}

/**
 * Translated action data for a {@link pf1.documents.ItemPF}
 */

declare interface TranslatedAction {
  attackName?: string;
  attackNotes?: string[];
  conditionals?: { name: string }[];
  description?: string;
  duration?: string;
  effectNotes?: string[];
  name?: string;
  range?: string;
  save?: string;
  spellArea?: string;
  spellEffect?: string;
  target?: string;
}
