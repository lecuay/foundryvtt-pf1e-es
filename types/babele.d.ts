declare interface Babele {
  register(options: {lang: string, module: string, dir: string}): void;
  registerConverters(options: {converters: Record<string, (data: any) => any>}): void;
}

// Add 'babele' property to 'game' global
declare interface Game extends Game {
  babele: Babele | undefined;
}
