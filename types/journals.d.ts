declare interface TranslatedJournalPage {
  name: string;
  text: string;
}

declare interface TranslatedJournalEntry {
  name: string;
  pages: Record<string, TranslatedJournalPage>;
}

declare interface TranslatedJournalCompendium {
  label: string;
  folders?: Record<string, string>;
  entries?: Record<string, TranslatedJournalEntry>;
}
