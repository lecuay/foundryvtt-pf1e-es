/**
 * @typedef FileTranslationStatisticObj
 * @prop {number} translated The number of translated entries.
 * @prop {number} untranslated The number of entries missing translation.
 * @prop {number} total The total of entries.
 * @prop {string} percentage The percentage translated.
 * @prop {string} [compendiumName] The name of the translated compendium.
 * @prop {string} moduleDomain To which module is this file associated to.
 *
 * @typedef {Object.<string, FileTranslationStatisticObj> & {totalPercentage: string}} TranslationStatisticObj
 *
 */

const path = require("path");

const { readFile, readTranslationsFolder } = require("./files.cjs");

/**
 *
 * @param {string} fileURL File with the translations.
 * @returns {Promise.<FileTranslationStatisticObj>} The object with all translations.
 */
const getFileTranslationCount = async (fileURL) => {
  if (!fileURL) {
    throw new Error("A file needs to be passed!");
  }

  console.info(`Reading items in ${fileURL}`);
  const translationObj = readFile(fileURL);
  const entries = translationObj.entries;
  const entriesMap = new Map(Object.entries(entries));

  /** @type {Array.<string>} */
  let untranslatedArray = new Array();
  const entriesMapKeys = Array.from(entriesMap.keys());
  /** @type {import("cld")} */
  let cld;
  try {
    cld = require("cld");
  } catch (error) {
    if (error.toString().match("Cannot find module 'cld'")) {
      console.error(
        "\x1b[31mYou don't have 'cld' installed. Please run \x1b[1mnpm install -g cld\x1b[0m"
      );
    }
    throw error;
  }

  const entriesWithoutDesc = [
    "pf1.armors-and-shields.json",
    "pf1.class-abilities.json",
    "pf1.feats.json",
    "pf1.spells.json",
    "pf1.weapons-and-ammo.json",
    "pf-content.pf-occult-rituals.json",
    "pf-content.pf-feats.json",
    "pf-content.pf-magic-items.json",
    "pf-content.pf-traits",
  ];

  await Promise.all(
    entriesMapKeys.map(async (key) => {
      let entry = entriesMap.get(key);
      if (key === entry.name) {
        if (entriesWithoutDesc.some((file) => fileURL.includes(file))) {
          if (!entry.description) {
            untranslatedArray.push(key);
          }
        } else if (
          entry.name.match(/(Patron|Subdomain)$/) &&
          key === entry.name
        ) {
          untranslatedArray.push(key);
        } else {
          /**
           * @type {string} Removes the compendium links for better
           * machine-understanding.
           */
          let cleanDescription = entry.description;
          const compendiumRegex = /@UUID\[.+\]\{(?<CompendiumText>.+)\}/;
          if (compendiumRegex.test(cleanDescription)) {
            const allCoincidences = Array.from(
              cleanDescription.matchAll(new RegExp(compendiumRegex.source, "g"))
            );
            for (let idx = 0; idx < allCoincidences.length; idx++) {
              const compendiumLink = allCoincidences[idx];
              const compendiumText = compendiumLink.groups?.CompendiumText;
              cleanDescription = cleanDescription.replace(
                new RegExp(`@UUID\\[.+\\]\\{${compendiumText}\\}`, "g"),
                compendiumText
              );
            }
          }

          try {
            const languageResult = await cld.detect(entry.description, {
              encodingHint: "UTF8UTF8",
              isHTML: true,
              languageHint: "es",
            });

            if (languageResult.languages.length > 1) {
              // Since there are some words translated via grep
              // (such as "Benefits") we need to filter to percentage
              const lanSpanish = languageResult.languages
                .filter((lan) => lan.code === "es")
                .pop();
              const lanEnglish = languageResult.languages
                .filter((lan) => lan.code === "en")
                .pop();
              if (
                // We only care if the two languages share a 75%-25% ratio or close
                (lanSpanish?.percent >= 25 &&
                  lanSpanish?.percent < 75 &&
                  lanEnglish?.percent !== 0) ||
                (lanEnglish?.percent >= 25 &&
                  lanEnglish?.percent < 75 &&
                  lanSpanish?.percent !== 0)
              ) {
                console.warn(
                  `"${key}" on ${fileURL} has more than one language: ${JSON.stringify(
                    languageResult.languages
                  )}`
                );
              }
            }

            const language = languageResult.languages[0];
            if (language?.code === "en") {
              untranslatedArray.push(key);
              if (!languageResult.reliable) {
                console.warn(
                  `"${key}" on ${fileURL} set as English but isn't reliable (Accuracy: ${language.percent})`
                );
              }
            }
          } catch (error) {
            console.error(`${error} -> ${key} (${fileURL})`);
            untranslatedArray.push(key);
          }
        }
      }
    })
  );

  const total = Object.keys(entries).length;
  const untranslated = untranslatedArray.length;
  const translated = total - untranslated;
  const percentage = `${((translated * 100) / total).toFixed(2)}%`;

  return {
    translated,
    untranslated,
    total,
    percentage,
    compendiumName: translationObj.label,
    moduleDomain: path.basename(fileURL).split(".")[0],
  };
};

/**
 * Returns the translations of all files.
 *
 * @param {Array.<string>} translationFiles List of files to get analytics from.
 * @returns {Promise.<{perFile: TranslationStatisticObj, perModule: TranslationStatisticObj}>} The final object with all statistics.
 */
const getTranslationsInfo = async (translationFiles = []) => {
  if (translationFiles.length == 0) {
    translationFiles = readTranslationsFolder();
  }

  /** @type {TranslationStatisticObj} */
  const tPerModule = {};

  /** @type {TranslationStatisticObj} */
  const translationObj = {};
  let totalEntries = 0;
  let totalTranslated = 0;
  for (const fileURL of translationFiles) {
    const objKey = fileURL.replace("./", "").replace("lang/es/", "");
    const fileTranslationObj = await getFileTranslationCount(fileURL);
    translationObj[objKey] = fileTranslationObj;
    totalEntries += fileTranslationObj.total;
    totalTranslated += fileTranslationObj.translated;
    const moduleKey = fileTranslationObj.moduleDomain;
    translationObj[objKey].moduleDomain = moduleKey;
    if (!Object.prototype.hasOwnProperty.call(tPerModule, moduleKey)) {
      tPerModule[moduleKey] = {
        total: 0,
        translated: 0,
        untranslated: 0,
        percentage: "",
      };
    }
    tPerModule[moduleKey].total += fileTranslationObj.total;
    tPerModule[moduleKey].translated += fileTranslationObj.translated;
    tPerModule[moduleKey].untranslated += fileTranslationObj.untranslated;
  }
  console.info(
    `There's a total of ${totalEntries} entries of which ${totalTranslated} are translated.`
  );
  translationObj.totalPercentage = `${(
    (totalTranslated * 100) /
    totalEntries
  ).toFixed(2)}%`;

  for (const moduleKey in tPerModule) {
    const tModule = tPerModule[moduleKey];
    tPerModule[moduleKey].percentage = `${`${(
      (tModule.translated * 100) /
      tModule.total
    ).toFixed(2)}%`}`;
  }

  return { perFile: translationObj, perModule: tPerModule };
};

const _printResult = ({ perFile, perModule }) => {
  if (perModule) {
    console.info("Translations by module\n--------------------------");
    console.info(perModule);
    console.info("--------------------------");
  }
  if (perFile) {
    console.info("Translations by file\n--------------------------");
    console.info(perFile);
    console.info("--------------------------");
  }
};

/**
 *
 * @param {TranslationStatisticObj} translations The JSON with translations.
 * @returns {string} Formatted markdown table.
 */
const writeTableOfTranslations = (translations) => {
  /**
   * |Compendium|Porcentaje|Entradas traducidas|
   * |:--------:|----------|-------------------|
   * |Armaduras y escudos|100%|64|
   * |...|...|...|
   */
  const tableHeader = `|Compendium|Porcentaje|Entradas traducidas|\n|:-:|-|-|`;
  /** @type {Object.<string, Array.<string>} */
  const compendiumEntries = {};

  // eslint-disable-next-line array-element-newline
  Object.entries(translations).forEach(([compendiumKey, translation]) => {
    // We need to prune the object to avoid `totalPercentage`
    if (compendiumKey !== "totalPercentage") {
      const { compendiumName, percentage, translated, moduleDomain } =
        translation;

      if (
        !Object.prototype.hasOwnProperty.call(compendiumEntries, moduleDomain)
      ) {
        compendiumEntries[moduleDomain] = [];
      }
      compendiumEntries[moduleDomain].push(
        `|${compendiumName}|${percentage}|${translated}|`
      );
    }
  });

  const labelMap = new Map([
    ["pf1", "Sistema Pathfinder 1e"],
    [
      "pf-content",
      "[Pathfinder 1e Content](https://foundryvtt.com/packages/pf-content)",
    ],
    [
      "pf1e-archetypes",
      "[Archetypes](https://gitlab.com/tristangoucher/pf1e-archetypes)",
    ],
    [
      "pf1-bestiary",
      "[Bestiaries](https://gitlab.com/foundryvtt_pathfinder1e/pf1-bestiary)",
    ],
  ]);
  let output = "";
  // eslint-disable-next-line array-element-newline
  Object.entries(compendiumEntries).forEach(([moduleDomain, tableList]) => {
    output += `\n\n\x1b[1m${labelMap.get(
      moduleDomain
    )}\x1b[0m\n${tableHeader}\n${tableList.sort().join("\n")}`;
  });

  return output;
};

if (require.main === module) {
  if (process.argv.length == 2) {
    getTranslationsInfo().then(_printResult);
  } else {
    const modules = process.argv
      .slice(2)
      .filter(
        (arg) =>
          !arg.includes("--") &&
          (arg === "pf1" || arg === "pf-content" || arg === "pf1e-archetypes")
      );
    let files = process.argv
      .slice(2)
      .filter((arg) => !arg.includes("--") && !modules.includes(arg));
    let flags = process.argv.slice(2).filter((arg) => arg.includes("--"));
    files = files.map((f) => {
      f = f.includes("lang/es/") ? f : `./lang/es/${f}`;
      f = f.endsWith(".json") ? f : `${f}.json`;
      return f;
    });
    files.push(
      ...readTranslationsFolder().filter((file) =>
        modules.some((moduleName) => file.includes(`${moduleName}.`))
      )
    );
    const translationInfo = getTranslationsInfo(files);

    if (flags.includes("--write-table")) {
      translationInfo.then(({ perFile, _perModule }) => {
        console.debug(writeTableOfTranslations(perFile));
      });
    } else {
      translationInfo.then(({ perFile, _perModule }) => {
        // If we are giving the files we don't want the translations per module
        _printResult({ perFile: perFile, perModule: undefined });
      });
    }
  }
}
