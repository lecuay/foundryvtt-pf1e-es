/**
 * @typedef Options
 * @prop {boolean} [logfile] Path to log file. If not provided it will not be created.
 * @prop {boolean} [noName] Whether to remove the name from the entry.
 * @prop {boolean} [remove] Whether to remove the entry. Not just log it.
 */

const { order } = require("../cleanup/orderEntries.cjs");
const { readFile, writeFile } = require("./files.cjs");

const path = require("path");
const fs = require("fs");

/**
 * This function will check the entries on f1 and add them with empty descriptions to f2.
 *
 * @param {string} f1 First file to merge.
 * @param {string | undefined} [f2=undefined] Second file to merge. If not provided it will assume it the same filename in `translations` folder.
 * @param {Options} [options={}] Options for the merge.
 * @returns {import("./files.cjs").TranslationsObject} New translations file
 */
const mergeTranslations = (f1, f2 = undefined, options = {}) => {
  // Automatically read from lang/en if only the name is given
  if (!f1.includes("/")) f1 = `./lang/en/${f1}`;
  const sourceTranslationFile = readFile(f1);
  if (!f2) {
    f2 = `./lang/es/${path.basename(f1)}`;
  }
  const localizedTranslationFile = readFile(f2);

  for (const key in sourceTranslationFile.entries) {
    // Set empty description
    sourceTranslationFile.entries[key].description = "";
    // Set empty name
    if (options.noName) {
      sourceTranslationFile.entries[key].name = "";
    } else {
      // If entry exists (not new file neither added) and name is empty, copy from original
      if (
        localizedTranslationFile.entries[key] &&
        !localizedTranslationFile.entries[key]?.name
      ) {
        localizedTranslationFile.entries[key].name =
          sourceTranslationFile.entries[key].name;
      }
    }
    // Set empty unidentifiedName if present
    if (sourceTranslationFile.entries[key].unidentifiedName) {
      sourceTranslationFile.entries[key].unidentifiedName = "";
    }
    // Set empty materialFocus if present
    if (sourceTranslationFile.entries[key].materialFocus) {
      sourceTranslationFile.entries[key].materialFocus = "";
    }
    // Set empty materials if present
    if (sourceTranslationFile.entries[key].materials) {
      sourceTranslationFile.entries[key].materials = "";
    }
    // Set empty unidentifiedDescription if present
    if (sourceTranslationFile.entries[key].unidentifiedDescription) {
      sourceTranslationFile.entries[key].unidentifiedDescription = "";
    }

    // Set empty text to context notes
    if (sourceTranslationFile.entries[key].contextNotes) {
      for (const note in sourceTranslationFile.entries[key].contextNotes) {
        sourceTranslationFile.entries[key].contextNotes[note].text = "";
      }
    }

    // Set empty text to fields in actions
    if (sourceTranslationFile.entries[key].actions) {
      for (const action in sourceTranslationFile.entries[key].actions) {
        if (sourceTranslationFile.entries[key].actions[action].target) {
          sourceTranslationFile.entries[key].actions[action].target = "";
        }

        if (sourceTranslationFile.entries[key].actions[action].save) {
          sourceTranslationFile.entries[key].actions[action].save = "";
        }

        if (sourceTranslationFile.entries[key].actions[action].name) {
          sourceTranslationFile.entries[key].actions[action].name = "";
        }

        if (sourceTranslationFile.entries[key].actions[action].duration) {
          sourceTranslationFile.entries[key].actions[action].duration = "";
        }

        if (sourceTranslationFile.entries[key].actions[action].spellArea) {
          sourceTranslationFile.entries[key].actions[action].spellArea = "";
        }

        if (sourceTranslationFile.entries[key].actions[action].description) {
          sourceTranslationFile.entries[key].actions[action].description = "";
        }

        if (sourceTranslationFile.entries[key].actions[action].effectNotes) {
          for (const note in sourceTranslationFile.entries[key].actions[action]
            .effectNotes) {
            sourceTranslationFile.entries[key].actions[action].effectNotes[
              note
            ] = "";
          }
        }

        if (sourceTranslationFile.entries[key].actions[action].conditionals) {
          for (const cond in sourceTranslationFile.entries[key].actions[action]
            .conditionals) {
            sourceTranslationFile.entries[key].actions[action].conditionals[
              cond
            ] = "";
          }
        }
      }
    }
  }

  // Remove entries that are no longer part of system
  // Storing deleted entries for debugging purposes
  /** @type {Record<string, {originalName: string}>} New entries added */
  const newEntries = {};
  /** @type {Record<string, {originalName: string}>} Deleted entries */
  const deletedEntries = {};

  // Checking new entries in source not reflected in localized
  for (const key in sourceTranslationFile.entries) {
    if (!localizedTranslationFile.entries[key]) {
      localizedTranslationFile.entries[key] =
        sourceTranslationFile.entries[key];
      newEntries[key] = {
        originalName: sourceTranslationFile.entries[key].name,
      };
    }
  }

  // Checking entries in localized removed from source
  for (const key in localizedTranslationFile.entries) {
    if (!sourceTranslationFile.entries[key]) {
      deletedEntries[key] = {
        originalName: localizedTranslationFile.entries[key].name,
      };
    }
  }

  if (Object.keys(newEntries).length > 0) {
    console.debug(
      `\n\x1b[36mNew entries (${Object.keys(newEntries).length}):\x1b[0m ${Object.entries(
        newEntries
      )
        .map(([id, _value]) => `\x1b[36m${id}\x1b[0m`)
        .join(" | ")}`
    );
  }
  if (Object.keys(deletedEntries).length > 0) {
    console.debug(
      `\n\x1b[31m\x1b[1mTo remove entries (${deletedEntries.length}):\x1b[0m ${Object.entries(
        deletedEntries
      )
        .map(([id, value]) => `\x1b[31m${id} [${value.originalName}]\x1b[0m`)
        .join(" | ")}`
    );

    // Write logfile
    if (options.logfile) {
      fs.writeFileSync(
        `${new Date().toISOString().slice(0, 10).replace(/-/g, "")}_${path.basename(f2).replace(".json", "")}_toAdd.log`,
        Object.entries(newEntries)
          .map(([id, _value]) => `${id}`)
          .join("\n")
      );
      fs.writeFileSync(
        `${new Date().toISOString().slice(0, 10).replace(/-/g, "")}_${path.basename(f2).replace(".json", "")}_toRemove.log`,
        Object.entries(deletedEntries)
          .map(([id, value]) => `${id} [${value.originalName}]`)
          .join("\n")
      );
    }

    // Remove entries
    if (options.remove) {
      console.debug(
        `\n\x1b[31m\x1b[1mRemoving ${Object.keys(deletedEntries).length} entries...\x1b[0m`
      );

      for (const id in deletedEntries) {
        delete localizedTranslationFile.entries[id];
      }
    }
  }

  const orderedTranslationFile = order(localizedTranslationFile);
  writeFile(f2, orderedTranslationFile);
};

if (require.main === module) {
  if (process.argv.length < 3) {
    console.error("You need to declare the two files to merge.");
    process.exit(1);
  }
  // eslint-disable-next-line array-element-newline
  const [f1, f2] = process.argv
    .filter((v) => !v.startsWith("-"))
    .slice(2, 4)
    .map((v) => (v.endsWith(".json") ? v : `${v}.json`));
  // Add options as an object
  /** @type {Options} List of options */
  const options = {
    logfile: false,
  };
  process.argv
    .filter((v) => v.startsWith("-"))
    .map((v) => v.replace(/^--?/, ""))
    .reduce((obj, v) => {
      obj[v.split("=")[0]] = v.split("=")[1] || true;
      return obj;
    }, options);
  mergeTranslations(f1, f2, options);
}
