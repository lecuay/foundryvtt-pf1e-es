#! /bin/env bash

INFO='\e[36m'
ERROR='\e[31m'
SUCCESS='\e[32m'
WARNING='\e[33m'
BOLD='\e[1m'
CLEAR='\e[0m'

echo -e "${INFO}Building project...${CLEAR}"
if ! command -v npm &> /dev/null
then
    echo -e "${ERROR}Seems like you don't have npm installed. Please install it.${CLEAR}"
    exit 1
fi

npm run build

echo -e "${INFO}Installing project in module...${CLEAR}"
read -p "$(echo -e "${BOLD}Enter FoundryVTT Data Dir [${HOME}/.local/share/FoundryVTT/Data] ${CLEAR}")" FOUNDRY_DATA_DIR
FOUNDRY_DATA_DIR=${FOUNDRY_DATA_DIR:-${HOME}/.local/share/FoundryVTT/Data}
if [ ! -d ${FOUNDRY_DATA_DIR} ];
then
    echo -e "${ERROR}Given directory doesn't exist.${CLEAR}"
    exit 1
fi

PF1E_ES_DIR=${FOUNDRY_DATA_DIR}/modules/pf1e-es

echo -e "${CYAN}Installing module pf1e-es in ${FOUNDRY_DATA_DIR}/modules...${CLEAR}"
if [ -d ${PF1E_ES_DIR} ];
then
    read -p "$(echo -e "${BOLD}Seems like folder already exists. Delete? [Y] ${CLEAR}")" DELETE_FOLDER
    DELETE_FOLDER=${DELETE_FOLDER:-Y}
    if [[ $DELETE_FOLDER =~ [Yy] ]];
    then
        echo -e "${INFO}Removing old pf1e-es module...${CLEAR}" && rm -rf $PF1E_ES_DIR
    elif [[ $DELETE_FOLDER =~ [Nn] ]];
    then
        echo -e "${WARNING}We can't install the new module if the old one is still installed.${CLEAR}"
        exit 1
    else
        echo -e "${ERROR}Option not valid!${CLEAR}"
        exit 1
    fi
fi

echo -e "${INFO}Symlink the module...${CLEAR}"
ln -s $(pwd)/dist/ ${PF1E_ES_DIR}

read -p "$(echo -e "${BOLD}Symlink translations (useful for development)? [Y] ${CLEAR}")" SYMLINK_TRANSLATION
SYMLINK_TRANSLATION=${SYMLINK_TRANSLATION:-Y}
if [[ $SYMLINK_TRANSLATION =~ [Yy] ]];
then
    echo -e "${INFO}Creating symbolic links for translations...${CLEAR}"
    rm -r $(pwd)/dist/translations
    ln -s $(pwd)/lang/es/ $(pwd)/dist/translations
fi

read -p "$(echo -e "${BOLD}Symlink code (useful for debugging automatic translations)? [N] ${CLEAR}")" SYMLINK_CODE
SYMLINK_CODE=${SYMLINK_CODE:-N}
if [[ $SYMLINK_CODE =~ [Yy] ]];
then
    echo -e "${INFO}Creating symbolic links for code...${CLEAR}"
    # Check if there's a dist folder
    if [ -d $(pwd)/dist ]; then
        rm -r $(pwd)/dist/*.mjs
    fi

    for file in $(pwd)/scripts/*.mjs; do
        dest=$(pwd)/dist/$(basename -- $file)
        echo -e "${CYAN}Linking ${file} -> ${dest}${END}"
        ln $file $dest
    done
fi

echo -e "${SUCCESS}All ready! Restart your server!${CLEAR}"
