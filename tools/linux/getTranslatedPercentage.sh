#! /bin/env bash

INFO='\e[36m'
ERROR='\e[31m'
SUCCESS='\e[32m'
WARNING='\e[33m'
BOLD='\e[1m'
CLEAR='\e[0m'

echo -e "${CYAN}Getting changed files and their percentages since last release${END}"
npm run stats $(git diff --name-only $(git describe --abbrev=0 --tags) | grep lang\/es\/)
