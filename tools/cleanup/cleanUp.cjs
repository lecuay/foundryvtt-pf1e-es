const { readFile, writeFile } = require("../utils/files.cjs");

/**
 * Transform the descriptions of non translated entries into empty string in order to get the fallback.
 *
 * @param {string} fileURL The file to be cleaned up.
 * @param {Array.<string>} whiteList A list of references to be ignored by the system. This is often
 * used for entries that have the same name on translated and non translated versions
 */
const cleanUp = (fileURL, whiteList = []) => {
  if (!fileURL) {
    throw new Error("A file needs to be passed!");
  }

  console.info(`Cleaning up file ${fileURL}`);
  if (whiteList.length != 0) {
    console.info(`Using the following whitelisted names: ${whiteList}`);
  }
  let translationObj = readFile(fileURL);
  let entries = translationObj.entries;
  const entriesMap = new Map(Object.entries(entries));

  /** @type {Array.<string>} */
  let untranslated = new Array();

  entriesMap.forEach((translationObj, key, _map) => {
    if (key == translationObj.name) {
      if (!whiteList.includes(key)) {
        untranslated.push(translationObj.name);
      }
    }
  });

  console.log(`Total entries: ${Object.keys(entries).length}`);
  console.log(`Missing translation: ${untranslated.length}`);

  console.info(
    `Deleting description from ${untranslated.length} non translated entries...`
  );
  untranslated.forEach((value, _index, _array) => {
    entries[value].description = "";
  });
  translationObj.entries = entries;

  writeFile(fileURL, translationObj);
};

if (require.main === module) {
  const args = process.argv;
  const fileURL = args[2];
  cleanUp(fileURL);
}

module.exports = { cleanUp };
