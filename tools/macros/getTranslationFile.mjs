/**
 * @typedef ParseOptions
 * @prop {boolean} [addMapping] - Whether to add the mapping to the translation file.
 * @prop {boolean} [autoTranslateActionsNames] - Whether to translate actions.
 * @prop {boolean} [useIds] - Whether to use IDs for translations.
 */

// Foundry Compendium Type ["Actor", "Cards", "Item", "JournalEntry", "Macro", "Playlist", "RollTable", "Scene", "Adventure"]
const supported = ["Item", "JournalEntry"];
// There's an error with Journals so it should only be exportable in English
if (game.i18n.lang === "en") supported.push("JournalEntry");

/** @type {ParseOptions} */
const defaultOptions = {
  addMapping: game.i18n.lang !== "en",
  autoTranslateActionsNames: true,
  useIds: false,
};

/** List of action names that are automatically handled by i18n */
const actionNamesFromI18n = Object.keys(game.i18n.translations.PF1)
  // We only want one word names
  .filter((s) => s.length - s.replace(/[A-Z]/g, "").length === 1)
  // We don't want weird characters
  .filter((s) => !s.includes("/"))
  .map((name) => game.i18n.localize(`PF1.${name}`));

/**
 * Transform raw actions into something translation friendly.
 *
 * @param {Array<ItemActionRaw>} actions - Array of actions associated to the item.
 * @param {boolean} [autoTranslateActionsNames] - Whether to translate actions names.
 *
 * @returns {Array<TranslatedAction>} - Array of translations for the actions.
 */
const parseActions = (actions, autoTranslateActionsNames) => {
  return actions.map((action) => {
    /** @type {TranslatedAction} */
    const translationFriendlyAction = {};
    translationFriendlyAction.attackName = action.attackName;
    translationFriendlyAction.attackNotes = action.attackNotes;

    if (action.conditionals) {
      translationFriendlyAction.conditionals = Object.fromEntries(
        action.conditionals.map((cond) => ["name", cond.name])
      );
    }

    translationFriendlyAction.description = action.description;
    translationFriendlyAction.duration =
      action.duration?.units === "spec" ? action.duration?.value : undefined;
    translationFriendlyAction.effectNotes = action.effectNotes;

    // If the action name is registered in i18n, actions converter should be using that
    if (autoTranslateActionsNames) {
      translationFriendlyAction.name = actionNamesFromI18n.includes(action.name)
        ? undefined
        : action.name;
    } else {
      translationFriendlyAction.name = action.name;
    }

    translationFriendlyAction.range =
      action.range?.units === "spec" ? action.range?.value : undefined;
    translationFriendlyAction.save = action.save?.description;
    translationFriendlyAction.spellArea = action.area;
    translationFriendlyAction.spellEffect = action.spellEffect;
    translationFriendlyAction.target = action.target?.value;

    // Now we remove undefined values
    Object.keys(translationFriendlyAction).forEach((key) => {
      if (translationFriendlyAction[key] === undefined) {
        delete translationFriendlyAction[key];
      }
    });

    return translationFriendlyAction;
  });
};

/**
 * Transform raw context notes into something translation friendly.
 *
 * @param {Array<ContextNote>} contextNotes - List of original Context Notes
 * @returns {Array<TranslatedContextNote>} - List of Context Notes translated
 */
const parseContextNotes = (contextNotes) => {
  return contextNotes.map((ctxNote) => {
    /** @type {TranslatedContextNote} */
    const translationFriendlyNote = {};
    translationFriendlyNote.text = ctxNote.text;
    return translationFriendlyNote;
  });
};

/**
 * Transform raw script calls into something translation friendly.
 *
 * @param {Array<ScriptCallRaw>} scriptCalls - List of original Script Calls
 * @returns {Array<TranslatedScriptCall>} - List of Script Calls translated
 */
const parseScriptCalls = (scriptCalls) => {
  return scriptCalls.map((scrCall) => {
    /** @type {TranslatedScriptCall} */
    const translationFriendlyCall = {};
    translationFriendlyCall.name = scrCall.name;
    return translationFriendlyCall;
  });
};

/**
 * Extract a pack of Items (Item pack type).
 *
 * @param {CompendiumCollection} - The pack to extract.
 * @param {ParseOptions} options - Additional options
 *
 * @returns {Promise<Record<string, any>>} Translation object.
 */
const extractItemPack = async (pack, options) => {
  /** @type {Collection<Document>} */
  const indexedPack = await pack.getIndex({
    fields: [
      "system.actions",
      "system.alignment",
      "system.contextNotes",
      "system.description",
      "system.materials",
      "system.unidentified",
      "type",
    ],
  });

  // Translation object
  const translationJSON = {
    label: pack.metadata?.label,
    folders: {},
    mapping: {},
    entries: {},
  };

  Array.from(indexedPack.values()).forEach((d) => {
    // First we get the Babele ID (the name basically)
    let babeleID;
    if (options.useIds) {
      babeleID = d._id;
    } else {
      babeleID = d.originalName || d.name;
    }
    let entry = {};

    // Name and description are pretty straightforward
    entry = {
      name: d.name,
      description: d.system?.description?.value || "",
    };

    // Adding unidentified name if present
    if (d.system?.unidentified) {
      entry.unidentifiedName = d.system.unidentified.name;

      // Adding default mapping
      if (options.addMapping) {
        translationJSON.mapping["unidentifiedName"] =
          "system.unidentified.name";
      }
    }

    // Adding unidentified description if present
    if (d.system?.description?.unidentified) {
      entry.unidentifiedDescription = d.system.description.unidentified;

      // Adding default mapping
      if (options.addMapping) {
        translationJSON.mapping["unidentifiedDescription"] =
          "system.description.unidentified";
      }
    }

    // Classes have alignment
    if (d.system?.alignment) {
      entry.alignment = d.system.alignment;

      // Adding default mapping
      if (options.addMapping) {
        translationJSON.mapping["alignment"] = "system.alignment";
      }
    }

    // Instructions field
    if (d.system?.description?.instructions) {
      entry.instructions = d.system.description.instructions;

      // Adding default mapping
      if (options.addMapping) {
        translationJSON.mapping["instructions"] =
          "system.description.instructions";
      }
    }

    // Actions need special handling
    const itemActions = d.system?.actions || [];
    if (itemActions.length > 0) {
      console.debug(`Parsing actions for ${babeleID} (${itemActions.length})`);
      entry.actions = parseActions(
        itemActions,
        options.autoTranslateActionsNames
      );

      // Remove empty objects
      entry.actions = entry.actions.filter((a) => Object.keys(a).length > 0);
      // Remove actions if empty after cleaning
      if (entry.actions.length === 0) {
        delete entry.actions;
      }

      // Adding default mapping
      if (options.addMapping) {
        translationJSON.mapping["actions"] = {
          path: "system.actions",
          converter: "actions",
        };
      }
    }

    // Context notes also need special handling
    const contextNotes = d.system?.contextNotes || [];
    if (contextNotes.length > 0) {
      console.debug(
        `Parsing context notes for ${babeleID} (${contextNotes.length})`
      );
      entry.contextNotes = parseContextNotes(contextNotes);

      // Adding default mapping
      if (options.addMapping) {
        translationJSON.mapping["contextNotes"] = {
          path: "system.contextNotes",
          converter: "contextNotes",
        };
      }
    }

    // Script calls need special handling
    const scriptCalls = d.system?.scriptCalls || [];
    if (scriptCalls.length > 0) {
      console.debug(
        `Parsing script calls for ${babeleID} (${scriptCalls.length})`
      );
      entry.scriptCalls = parseScriptCalls(scriptCalls);

      // Adding default mapping
      if (options.addMapping) {
        translationJSON.mapping["scriptCalls"] = {
          path: "system.scriptCalls",
          converter: "scriptCalls",
        };
      }
    }

    // Handling spells special fields
    if (d.type === "spell" && d.system?.materials) {
      console.debug(`Parsing materials for ${babeleID}`);
      entry.materials = d.system.materials.value;
      entry.materialsFocus = d.system.materials.focus;

      // Adding default mapping
      if (options.addMapping) {
        translationJSON.mapping["materials"] = "system.materials.value";
        translationJSON.mapping["materialsFocus"] = "system.materials.focus";
      }
    }

    translationJSON.entries[babeleID] = entry;
  });

  return translationJSON;
};

/**
 * Extract a pack of JournalEntries (JournalEntry pack type).
 *
 * @param {CompendiumCollection} pack The pack to extract.
 * @param {ParseOptions} options - Additional options
 *
 * @returns {Promise<Record<string, any>>}
 */
// eslint-disable-next-line no-unused-vars
const extractJournalEntryPack = async (pack, options) => {
  /** @type {Array.<JournalEntry>} */
  const journals = await pack.getDocuments();

  // Translation object
  /**
   * @type {TranslatedJournalCompendium}
   */
  const translationJSON = {
    label: pack.metadata?.label,
    folders: {},
    entries: {},
  };

  journals.forEach((cJournal) => {
    // First we get the Babele ID (the name basically)
    let babeleID;
    if (options.useIds) {
      babeleID = cJournal._id;
    } else {
      babeleID = cJournal.flags?.babele?.originalName || cJournal.name;
    }
    /** @type {TranslatedJournalEntry} */
    let entry = {};

    // Name is straightforward
    entry = {
      name: cJournal.name,
      pages: {},
    };

    cJournal.pages.forEach((cPage) => {
      entry.pages[cPage.name] = {
        name: cPage.name,
        text: cPage.text?.content || "",
      };
    });

    translationJSON.entries[babeleID] = entry;
  });

  return translationJSON;
};

/**
 * Get the translation file for the given pack.
 *
 * @param {string} packId The ID of the pack (compendium).
 * @param {ParseOptions} options - Additional options
 *
 * @returns {Promise<Record<string, any>>}
 *
 */
const packToTranslationFileJSON = async (packId, options) => {
  /** @type {CompendiumCollection | undefined } */
  const pack = game.packs.get(packId);
  console.info(`Processing "${packId}"`);
  if (!pack) {
    throw new Error(`Pack "${packId}" not found.`);
  }

  let translationJSON;

  switch (pack.metadata?.type) {
    case "Item":
      translationJSON = await extractItemPack(pack, options);
      break;
    case "JournalEntry":
      translationJSON = await extractJournalEntryPack(pack, options);
      break;
    default:
      throw new Error(
        `Pack "${packId}" is not one of the supported types (${supported.join(", ")}).`
      );
  }

  // Add folders
  if (pack.folders.size > 0) {
    translationJSON.folders = pack.folders.reduce((acc, folder) => {
      acc[folder._source.name] = folder.name;
      return acc;
    }, {});

    // Order the folders
    translationJSON.folders = Object.fromEntries(
      Object.entries(translationJSON.folders).sort((a, b) => {
        return a[0].localeCompare(b[0]);
      })
    );
  }

  // Ordering by key (original name) both entries and folders
  translationJSON.entries = Object.fromEntries(
    Object.entries(translationJSON.entries).sort((a, b) => {
      return a[0].localeCompare(b[0]);
    })
  );

  // Remove folder if empty
  if (
    translationJSON.folders &&
    Object.keys(translationJSON.folders).length === 0
  ) {
    delete translationJSON.folders;
  }
  // Remove mapping if empty
  if (
    translationJSON.mapping &&
    Object.keys(translationJSON.mapping).length === 0
  ) {
    delete translationJSON.mapping;
  }

  return translationJSON;
};

/**
 * Download the translation file for the given pack.
 *
 * @param {string} pack - The pack to download.
 * @param {ParseOptions} options - Options for the download.
 */
const downloadTranslationFile = async (pack, options) => {
  const translationFile = await packToTranslationFileJSON(pack, options);
  saveDataToFile(
    JSON.stringify(translationFile, null, 2) + "\n",
    "application/json",
    `${pack}.json`
  );
};

const htmlForm = `
<form class="flexcol">
  <section class="flex1">
    <label for="pack">
      Select pack
    </label>
    <select name="pack" id="pack">
      <option value="pf1.">All PF1e System Compendiums</option>
      <option value="pf-content.">All PF-Content Compendiums</option>
      <option value="pf1e-archetypes.">All PF1e Archetypes Compendiums</option>
      ${Array.from(game.packs.entries())
        .filter(([key, pack]) => key && supported.includes(pack.metadata?.type))
        .sort((a, b) =>
          a[1].metadata?.label.localeCompare(b[1].metadata?.label)
        )
        .map(
          ([key, pack]) =>
            `<option value="${key}">${game.i18n.localize(pack.metadata?.label) || key} (${pack.metadata?.packageName || game.i18n.localize("Unknown")})</option>`
        )
        .join("\n")}
    </select>
  </section>
  <section class="flex1">
    <section class="flex3">
      <label for="add-mapping">
        Add mapping
      </label>
      <input
        type="checkbox"
        name="add-mapping"
        id="add-mapping"
        ${defaultOptions.addMapping ? "checked" : ""}
      >
    </section>
    <section class="flex3">
      <small>
        <em>Recommended to disable if this is for Weblate sources. So they don't get overwritten.</em>
      </small>
    </section>
  </section>
  <section class="flex1">
    <section class="flex3">
      <label for="auto-action-names">
        Auto translate actions
      </label>
      <input
        type="checkbox"
        name="auto-action-names"
        id="auto-action-names" ${defaultOptions.autoTranslateActionsNames ? "checked" : ""}
      >
    </section>
    <section class="flex3">
      <label for="use-ids">
        Use IDs as keys
      </label>
      <input
        type="checkbox"
        name="use-ids"
        id="use-ids" ${defaultOptions.useIds ? "checked" : ""}
      >
    </section>
    <section class="flex3">
      <small>
        <em>Use Item IDs as JSON keys instead of names.</em>
      </small>
    </section>
    <!--
      <section class="flex3">
        <small>
          <em>The following action will be translated: ${actionNamesFromI18n.join(", ")}</em>
        </small>
      </section>
    -->
    <section class="flex3">
      <small>
        <em>
          Recommended if you have a system to automatically get translations from PF1.&lt;actionName&gt;
        </em>
      </small>
      <details>
        <summary>
          The following actions names will be ignored on the translation file:
        </summary>
        ${actionNamesFromI18n
          .filter((s) => !s.includes("PF1."))
          .sort((a, b) => a.localeCompare(b, game.i18n.lang))
          .join(", ")}
      </details>
    </section>
  </section>
</form>
`;

new Dialog({
  title: "Get Translation File",
  content: htmlForm,
  buttons: {
    ok: {
      icon: '<i class="fas fa-save"></i>',
      label: game.i18n.localize("Save"),
      callback: async (html) => {
        const pack = html.find("#pack")[0].value;
        const addMapping = html.find("#add-mapping")[0].checked;
        const autoActionsNames = html.find("#auto-action-names")[0].checked;
        const useIds = html.find("#use-ids")[0].checked;

        /** @type {ParseOptions} */
        const options = {
          ...defaultOptions,
          addMapping,
          autoTranslateActionsNames: autoActionsNames,
          useIds: useIds,
        };

        // Check if all pf, all pf-content or all pf1e-archetypes
        if (
          pack === "pf1." ||
          pack === "pf-content." ||
          pack === "pf1e-archetypes."
        ) {
          const packsToDownload = Array.from(game.packs.entries())
            .filter(
              ([key, thisPack]) =>
                key.startsWith(pack) &&
                supported.includes(thisPack.metadata?.type)
            )
            .map(([key, _thisPack]) => key);
          for (const pack of packsToDownload) {
            await downloadTranslationFile(pack, options);
          }
        } else {
          await downloadTranslationFile(pack, options);
        }
      },
    },
  },
  default: "ok",
}).render(true);
